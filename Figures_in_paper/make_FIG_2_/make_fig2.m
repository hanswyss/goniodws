R=4.5e-3; %Radius of cylindrical cell: 4.5 mm
anglebin=90; %angle bin (degrees)

close all
colors='brgmkcybrgmkcybrgmkcy';
symbols='od+xsod+xsod+xsod+xsod+xs';

for ii=1:5
    lstar(ii)=0.01*2^(ii-1)
end
global A_file;
clear legend_
figure(2)



for ii=1:5
    lstar_over_R=lstar(ii);
    lstar_=R*lstar_over_R;
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(180),'.mat'];
    if exist(filename)==2
        load(filename);
        
        binwidth=A_file(3,anglebin);
        average_s=R*A_file(2,anglebin);
        average_s_(ii)=average_s;
        I_tot=A_file(1,anglebin);
        Ps=A_file(4:303,anglebin)./I_tot./binwidth./R;
        s=((1:300).*binwidth.*R)';
        legend_{ii}=['$l^{\star}/R$ = ',num2str(lstar_over_R)];
        sumPs(ii)=sum(Ps)*binwidth*R;
    else
        Ps=0; s=0;
        ['File ',filename,' does not exist.']
    end
    subplot(2,2,1)
    format_string=[colors(ii),'-'];
    loglog(s,smooth(Ps,3),format_string)
    hold on
    axis([1e-2 100 1e-2 100])
    
    
    %Figure 2: Normalized by average path length:
    
    subplot(2,2,2)
    loglog(s./average_s,smooth(Ps.*average_s,3))
    hold all
end
%%
% figure(1)
% legend(legend_)
% DLSDWSdata.nicify_plot(gcf,['$P(s)$ vs. $s$ for different values of $l^{\star}/R$  ($\theta=$',num2str(anglebin),'$^\circ$).'],'$s$ [m]','$P(s)$');
% axis([4e-3 1 1e-1 100]);
% saveas(gcf, ['Ps_vs_s_',num2str(anglebin),'deg'], 'pdf')
% saveas(gcf, ['Ps_vs_s_',num2str(anglebin),'deg'], 'fig')

%%
% figure(2)
% legend(legend_)
% DLSDWSdata.nicify_plot(gcf,['$P(s) \cdot \tilde{s}$ vs. $s/\tilde{s}$ for different values of $l^{\star}/R$  ($\theta=$',num2str(anglebin),'$^\circ$).'],'$s / \tilde{s}$ [m]','$P(s) \cdot \tilde{s}$');
% axis([1e-1 10 1e-3 10]);
% saveas(gcf, ['Ps_vs_s_mastercurve',num2str(anglebin),'deg'], 'pdf')
% saveas(gcf, ['Ps_vs_s_mastercurve',num2str(anglebin),'deg'], 'fig')

%%  Average number of scattering events vs. l*/R
subplot(2,2,3)

x=lstar';y=(average_s_./(lstar.*R))';b=1;m=-2;
f = fit(x,y,'b*x^m')
fitx=logspace(-3,3,100);
subplot3fith=loglog(fitx,f.b.*fitx.^f.m,'k:');
for ii=1:5
    format_string=[colors(ii),symbols(ii)];
    loglog(lstar(ii),average_s_(ii),format_string)
    hold on
end
x=lstar';y=(average_s_)';b=1;m=-2;
f = fit(x,y,'b*x^m');
fitx=logspace(-3,3,100);
subplot3fith=loglog(fitx,f.b.*fitx.^f.m,'k--');
axis([6e-3 0.3 1e-2 100])
legend(subplot3fith,['fit: exponent $m=$',num2str(f.m)])
%% Average path length as a function of the distance D between entry and exit points
% (for fixed l* value; choose l*/R=0.08)
colors='brgmkcybrgmkcybrgmkcy';
symbols='od+xsod+xsod+xsod+xsod+xs';
%format_string=[colors(format_nr),symbols(format_nr)];
%
for ii=1:5
    lstar_over_R=0.01*2^(ii-1);
    lstar_=R*lstar_over_R;
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(180),'.mat'];
    if exist(filename)==2
        load(filename);
        binwidth=A_file(3,anglebin);
        angle_=(1:180)-0.5.*ones(1,180);
        s_av=R.*A_file(2,1:180);
        D_=R.*((1+cos(angle_./180.*pi)).^2+(sin(angle_./180.*pi)).^2).^0.5;
        
        
    else
        Ps=0; s=0;
        ['File ',filename,' does not exist.']
    end
    
    subplot(2,2,4)
    
     sel=D_>lstar_;
    format_string=[colors(ii),symbols(ii)];
    subfig4handles(ii)=loglog(D_(sel)./R,s_av(sel),format_string);
    hold on
    
   
    
    format_string=[colors(ii),'-'];
    loglog(D_(sel)./R,s_av(sel)./lstar_,format_string)
    axis([6e-3 3 0.01 200])
end


str1='$l^{\star}/R$=';
legend_={[str1,'0.01'],...
         [str1,'0.02'],...
         [str1,'0.04'],...
         [str1,'0.08'],...
         [str1,'0.16']};
legend(subfig4handles,legend_);

%% Nicify sub-plots:

ii=1;
ah=subplot(2,2,ii)
legend(legend_)
axis([2e-2 1000 6e-4 10])
xlabel_='$s$ [m]';
ylabel_='$P(s)$';
nicify_subplot(2,2,ii,xlabel_,ylabel_)


ii=2;
ah=subplot(2,2,ii)
legend(legend_)
axis([0.1 30 2e-3 2])
xlabel_='$s/\tilde{s}$';
ylabel_='$P(s) \cdot \tilde{s}$ [m]';
nicify_subplot(2,2,ii,xlabel_,ylabel_)

ii=3;
ah=subplot(2,2,ii)
axis([6e-3 0.3 1e-1 100])
xlabel_='$l^\star / R$';
ylabel_='$\tilde{s}$ [m]';
nicify_subplot(2,2,ii,xlabel_,ylabel_)
%legend(ah,'hide');

ii=4;
subplot(2,2,ii)
xlabel_='$D(\theta)/R$';
ylabel_='$\tilde{s}/l^\star$';
nicify_subplot(2,2,ii,xlabel_,ylabel_)

% export_fig('./fig2.pdf','-transparent')

