clear all
close all

colors='brgmkcybrgmkcybrgmkcy';
symbols='od+xsod+xsod+xsod+xsod+xs';

%%Concentrations and folder paths to data from dilutions A to D

% Dilution A : 50% of dilution1, [500 mL dil1 + 500 mL H2O] => Expected l*:  ~150 �m
% Dilution A2: 33% of dilution1 [330 mL dil1 + 670 mL H2O] => Expected l*:  ~227 �m
% Dilution B : 25% of dilution1, [250 mL dil1 + 750 mL H2O]  => Expected l*:  ~300 �m
% Dilution B2: 18% of dilution1 [180 mL dil1 + 820 mL H2O] => Expected l*:  ~417 �m
% Dilution C : 12.5 % of dilution1, [125 mL dil1 + 875 mL H2O]----- this is the current dilution 2 => Expected l*: ~600 �m
% Dilution C2: 9% of dilution1 [90 mL dil1 + 910 mL H2O] => Expected l*:  ~833 �m
% Dilution D : 6.25%% of dilution1, [62.5 mL dil1 + 937.5 mL H2O]  Expected l*:  ~1200 �m (here the diffusion approximation should only hold for small detection angles, i.e. distance between entry and exit points of photons ~ 10 mm..)

conc=[50;33;25;18;12.5;9;6.26]./100*0.05; %particle volume fractions (absolute)
sample_name={'Dilution A','Dilution A2','Dilution B','Dilution B2','Dilution C',...
             'Dilution C2','Dilution D'};
data_path={...
    './data/diluA/VH/',...
    './data/diluB/VH/',...
    './data/diluA2/VH/',...
    './data/diluB2/VH/',...
    './data/diluC/VH/',...
    './data/diluC2/VH/',...
    './data/diluD/VH/'};
basename={...
    'DiluA_VH00',...
    'DiluB_VH00',...
    'DiluA2_VH00',...
    'DiluB2_VH00',...
    'DiluC_VH00',...
    'DiluC2_VH00',...
    'DiluD_VH00'};







%% Radius of the sample cell:
R=0.0045;

%% import optimal lstar values

load('lstar_opt.mat')

% calculate average l_star values for each concentration:
for cc=1:7
    lstar_from_g1(cc)=mean(lstar_opt(cc,:));
end

% figure()
% plot(conc.^(-1),lstar_from_g1,'o')


%% extract data from ASC files:

endstring='_0001.ASC';
cc=1;aa=1;
ASCfile = inline('[data_path{cc},basename{cc},num2str_2digits(aa),endstring]','cc','aa','data_path','basename','endstring');

%pre-allocate:
theta_deg=zeros(7,13);
theta_rad=zeros(7,13);
avCR=zeros(7,13);
MonDiode=zeros(7,13);
I_exp=zeros(7,13);
s_av=zeros(7,13);

MonDiode_max=0;
for cc=1:7 %loop over 7 different concentrations
    for aa=1:13 %loop over 13 different angles
        fullname=ASCfile(cc,aa,data_path,basename,endstring);
        A=DLSDWSdata.ImportFromFile(fullname);

        % extract data:
         theta_deg(cc,aa)=A.angle_deg;
         theta_rad(cc,aa)=A.angle_rad;
         MonDiode(cc,aa)=A.MonitorDiode;
         avCR(cc,aa)=A.avCR;
         I_exp(cc,aa)=A.avCR/A.MonitorDiode;
         lambda=A.lambda_m;
         % Maximum Monitor diode reading (corresponding to 100%
         % transmission of the 50 mW incoming laser intensity):
         if A.MonitorDiode>MonDiode_max
             MonDiode_max=A.MonitorDiode;
         end
         
        
        % Calculate average path length (using previously calculated lstar
        % values:
        
         [s,Ps] = P_s_from_master_curve(R, lstar_opt(cc,aa), round(theta_deg(cc,aa)));
         s_av(cc,aa)=sum(s.*Ps)./sum(Ps);

    end
end


%% create main figure
mainh=figure()

%% sub-plot 2:

figure(mainh)
h_sp2=subplot(2,2,2)
for cc=7:(-1):1
    format_string=[colors(cc),symbols(cc),'-'];
    %semilogy(theta_deg(cc,:),I_exp(cc,:),format_string)
    semilogy(theta_deg(cc,:),avCR(cc,:)./MonDiode(cc,:).*MonDiode_max,format_string)
    hold on
    legend_{cc}=['$l^\star/R=',num2str(round(lstar_from_g1(cc)/R*1000)/1000),'$'];
end
axis([20 160 0.3 3000])
legend(legend_)
xlabel_='Detection angle $\theta$ [$^\circ$]';
ylabel_='$I_\mathrm{exp}$  [kHz]';
nicify_subplot(2,2,2,xlabel_,ylabel_)


%% sub-plot 1:
figure(mainh)
h_sp1=subplot(2,2,1)

%read data from simulation:
for ii=1:5
    lstar_sim(ii)=0.01*2^(ii-1);
end
for ii=5:(-1):1
    lstar_over_R=lstar_sim(ii);
    lstar_=R*lstar_over_R;
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(180),'.mat'];
    if exist(filename)==2
        load(filename);
        N_photons_file_(ii)=N_photons_file;
        for aa_sim=1:180
            binwidth=A_file(3,aa_sim);
            average_s=R*A_file(2,aa_sim);
            s_av_sim(ii,aa_sim)=average_s;
            I_sim(ii,aa_sim)=A_file(1,aa_sim);
            fraction_detected_sim(ii,aa_sim)=A_file(1,aa_sim)/N_photons_file_(ii);
            
            %Ps=A_file(4:303,anglebin)./I_tot./binwidth./R;
            %s=((1:300).*binwidth.*R)';
        end
        legend_{ii}=['$l^{\star}/R$ = ',num2str(lstar_over_R)];
        %sumPs(ii)=sum(Ps)*binwidth*R;
        format_string=[colors(ii+7),'-'];
        semilogy(20:160,fraction_detected_sim(ii,20:160),format_string)
        hold on
    else
        Ps=0; s=0;
        ['File ',filename,' does not exist.']
    end
   
    
end
axis([20 160 1e-11 1e-7])
legend(legend_)
xlabel_='Detection angle $\theta$ [$^\circ$]';
ylabel_='$I_\mathrm{sim}$ [a.u.]';
nicify_subplot(2,2,1,xlabel_,ylabel_)

%% sub-plot 3 - Intensity versus lstar (for different angles) - simulation and experiments
figure(mainh)
h_sp1=subplot(2,2,3)

beta_exp=2.8e10;

cnr=1;
for aa=13:(-1):1
    format_string=[colors(aa),symbols(aa)];
    %semilogy(theta_deg(cc,:),I_exp(cc,:),format_string)
    hcurve(cnr)=loglog(lstar_from_g1(:)./R,avCR(:,aa)./MonDiode(:,aa).*MonDiode_max,format_string);
    hold on
    legend_{cnr}=['$\theta=',num2str(theta_deg(1,aa)),'^\circ$'];
    cnr=cnr+1;
    I_exp_=avCR(:,aa)./MonDiode(:,aa).*MonDiode_max;
    %plot predicted intensity:
    format_string=[colors(aa),':'];
    for cc=1:7
        I_predicted_(cc,aa)=I_predicted(lstar_from_g1(cc)./R,beta_exp,theta_deg(cc,aa),fraction_detected_sim);
    end
    loglog(lstar_from_g1(:)./R,I_predicted_(:,aa),format_string)
    hold on

end
legend(hcurve,legend_)
axis([0.03 1 4 1000])
xlabel_='$l^\star/R$';
ylabel_='$I_\mathrm{exp}$ , $\beta_\mathrm{exp} \cdot I_\mathrm{sim}$ [kHz]';
nicify_subplot(2,2,3,xlabel_,ylabel_)

%% export main figure to pdf
% figure(mainh)
% export_fig('fig4.pdf','-transparent')