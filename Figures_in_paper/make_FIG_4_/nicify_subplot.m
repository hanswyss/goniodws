function ah = nicify_subplot(m,n,i,xlabel_,ylabel_)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
figure(gcf)
ah=subplot(m,n,i);



set(ah,'LineWidth',1);
set(ah,'FontSize',12);
xlabel(xlabel_,'Fontsize',20,'Interpreter','latex');
ylabel(ylabel_,'Fontsize',20,'Interpreter','latex');
%title(graph_title_,'Interpreter','latex');
% set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'Units', 'centimeters');
pos=get(gcf, 'Position');
%newsize=(pos(3)+pos(4))/2
scale=1.4;
pos(3)=24*scale;pos(4)=18*scale;
set(gcf, 'Position', pos);

pos=get(ah, 'Position');
newsize=(pos(3)+pos(4))/2;
pos(3)=newsize;pos(4)=newsize;
set(ah, 'Position', pos);

% set(ah, 'Units', 'centimeters');
% set(ah, 'Position', [3 3 12 12]);

% legend('Location','Best');
% legend(ah,'show');
% legend(ah,'boxoff');
% LEGH = legend;
% set(LEGH,'FontSize',10);
% set(LEGH,'Interpreter','latex');
set(get(ah,'Children'),'linewidth',1); % This sets the linewidth for all plots. (plots are 'Children' of the current axis.
% set(gcf,'PaperOrientation','landscape');
% set(gcf,'PaperPositionMode','auto');

legend(ah,'show');
%legend(ah,'boxoff');
LEGH = legend;
set(LEGH, 'Color', 'none');
set(LEGH,'FontSize',10);
set(LEGH,'Interpreter','latex');
set(LEGH,'Location','Best');


switch i
    
    case 1
        text(-0.2,1,'A','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 2
        text(-0.2,1,'B','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 3
        text(-0.2,1,'C','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 4
        text(-0.2,1,'D','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 5
        text(-0.2,1,'E','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 6
        text(-0.2,1,'F','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 7
        text(-0.2,1,'G','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 8
        text(-0.2,1,'H','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    
end

