function I = I_predicted(lstar_over_R,beta_exp,angle_,fraction_detected_sim)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for ii=1:5
    lstar_over_R_sim(ii)=0.01*2^(ii-1);
end
x=lstar_over_R_sim';
y=fraction_detected_sim(:,angle_);
logx=log(x);
logy=log(y);
f=polyfit(logx,logy,2);
%f = fit(x,y,'b*f = fit(x,y,'b*x^m'); plot(f,'k')x^m'); plot(f,'k')
logI=f(1)*log(lstar_over_R).^2+f(2)*log(lstar_over_R)+f(3);
I=exp(logI)*beta_exp;

end

