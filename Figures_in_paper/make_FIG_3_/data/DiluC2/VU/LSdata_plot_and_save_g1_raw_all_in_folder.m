% Copy this script into the directory where you have your .ASC light scattering
% files. Then run the script. The LSdata class file LSdata.m needs to be
% somewhere in your search path where Matlab can find it.

[scriptfolder,scriptname]=fileparts(mfilename('fullpath'));
cd(scriptfolder)
theFolder=scriptfolder;
close all
if exist(theFolder,'dir')==7
    ASCfiles=dir([theFolder,'/*.ASC']);
    nFiles=size(ASCfiles,1);
    if nFiles>0
        for ii=1:nFiles
            theFile=fullfile(theFolder,ASCfiles(ii).name);
            A=LSdata.ImportFromFile(theFile);
            figure(1)
            clf
            figure(1)
            %plot g1_raw:
            semilogx(A.t_s,A.g1_raw,'bo');
            graph_title_=ASCfiles(ii).name;
            xlabel_='$t$ [s]';
            ylabel_='$g_1(t)$ (raw)';
            axis([1e-8 A.duration/100 0 1.05]);
            LSdata.nicify_plot(gcf,graph_title_,xlabel_,ylabel_)
            outname=[ASCfiles(ii).name(1:(length(ASCfiles(ii).name)-4)),'.png'];
            saveas(gcf,outname);
        end
    else
        waitfor(msgbox('No files..'));
    end
end

