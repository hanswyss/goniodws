classdef LSdata
    % Class for light scattering data (instrument with 4 detectors)
    
    % Edits:
    % 2018-03-28: HW, fixed reading of angle data in LSdata.ImportfromFile.
    
    properties
        filename
        pathname
        t_ms % Correlation time in milliseconds
        t_s  % Correlation time in seconds
        corr % correlation (g2-1) for all channels
        g2min1 % g2-1
        g2
        g1_raw     % raw, measured g1
        g1         % g1 after correction, either scaled to 1 or Pusey averaged.
        g1_scaled % g1 scaled to an intercept of 1.
        Q      % Q value (in m^(-1))
        Q2     % Q^2 value in m^(-2)
        
        meanCR0 % 4 average countrates for the 4 detectors
        meanCR1
        meanCR2
        meanCR3
        avCR    % average of the (nonzero) countrates for the 4 detectors
        
        t_CR  % time column for countrate trace
        CR    % 4 countrate columns for countrate trace
        
        %other properties:
        
        MeasDate
        MeasTime
        Temp_K
        Temp_C
        visc_cp
        n_refr
        lambda_nm
        lambda_m
        angle_deg
        angle_rad
        duration
        Mode
        MonitorDiode
        
        % related to Pusey averaging:
        I_E_Pu1to3 % Array with intensities from 3 different Pusey measurements (countrate/monitor diode)
        I_E_Pu_av % Average of those 3 countrates (countrate/monitor diode)
        I_T       % Average intensity from current measurement (countrate/monitor diode)
        g1_Pu % Pusey-averaged g1
        g1_Pu_upper % Pusey averaged g1, using the highest of the three ensemble-average input intensities.
        g1_Pu_lower % Pusey averaged g1, using the lowest of the three ensemble-average input intensities.
        
        % related to mean-square displacement and microrheology
        msd_m2  % MSD in m^2
        msd_mu2 % MSD in micrometer^2
        a_tracer %
        
        omega  % frequency in radians per second
        alpha  % logarithmic slope of the MSD vs. t at time t (or omega=1/t)
        beta   % second-order logarithmic derivative of the MSD vs time
        Gs
        G_abs % Magnitude of the complex modulus in Pa
        Gp % Storage modulus in Pa
        Gpp    % Loss modulus in Pa
        G_raw     % Raw modulus value obtained from kT/pi/a_tracer./msd
        t_murheo % Corresponding time vector
        
        t_smooth  % smoothed time vector (same as t_murheo)
        msd_smooth % smoothed msd vector
        
        % related to file export, plots etc.
        output_string
        label
        
    end
    
    methods
        
        function IC = intercept_g1(A_,range)
            % range is the range of points over which an average is taken; for instance: range=1:20;
            IC=mean(A_.g1(range)');
        end
        
        function IC = intercept_g2(A_,range)
            % range is the range of points over which an average is taken; for instance: range=1:20;
            IC=mean(A_.g2(range)');
        end
        
        function IC = intercept_g2min1(A_,range)
            % range is the range of points over which an average is taken; for instance: range=1:20;
            IC=mean(A_.g2min1(range)');
        end
        
        function A = PuseyAverage(A_, PuFile1, PuFile2, PuFile3, intercept_range)
            
            A=A_;
            APu1=LSdata.ImportFromFile(PuFile1);APu2=LSdata.ImportFromFile(PuFile2);APu3=LSdata.ImportFromFile(PuFile3);
            I_E1=APu1.avCR/APu1.MonitorDiode;I_E2=APu2.avCR/APu2.MonitorDiode;I_E3=APu3.avCR/APu3.MonitorDiode;
            % Ensemble-averaged intensity:
            I_E_=(I_E1+I_E2+I_E3)/3;
            % Time-averaged intensity:
            I_T_=A_.avCR/A_.MonitorDiode;
            Y=I_E_/I_T_;
            
            Y_upper=max([I_E1,I_E2,I_E3])/I_T_;
            Y_lower=min([I_E1,I_E2,I_E3])/I_T_;
            
            
            intercept_=A.intercept_g2min1(intercept_range);
            
            A.g1_Pu=real((Y-1)/Y+((A_.g2-intercept_).^0.5)/Y);
            A.g1_Pu(A.g1_Pu==0)=NaN;
            A.g1=A.g1_Pu;
            
            A.g1_Pu_upper=real((Y_upper-1)/Y_upper+((A_.g2-intercept_).^0.5)/Y_upper);
            A.g1_Pu_upper(A.g1_Pu==0)=NaN;
            
            A.g1_Pu_lower=real((Y_lower-1)/Y_lower+((A_.g2-intercept_).^0.5)/Y_lower);
            A.g1_Pu_lower(A.g1_Pu==0)=NaN;
            A.I_E_Pu_av=I_E_;
 
        end
        
        function A = calc_G(A_,varargin)
            % This function calculates viscoelastic moduli from the motion
            % of tracer particles (of radius a) in the material.
            % The calculation is based on the equations given in the paper
            % Dasgupta et al. Journal of
            
            %Input option 1:
            % no additional input parameters; A = A.calc_G() or A = A.calc_G
            %Input option 2:
            % particle radius in m as additional input parameter; A = A.calc_G(0.3e-6)
            
            % a: particle radius in meters
            
            A=A_;
            
            switch nargin
                
                case 1 % no input arguments except the source object.
                    
                    if isempty(A.a_tracer)
                        message='Must provide a value for the tracer particle radius a (in m).';
                        h = msgbox( message );
                        return
                    end
                    
                case 2
                    if isempty(A.a_tracer)
                        A.a_tracer=varargin{1};
                    else
                        %message=['Overwriting the old value of the tracer particle radius ... ( a=',num2str(A.a_tracer),' ).'];
                        %h = msgbox( message );
                        A.a_tracer=varargin{1};
                    end
                    
                otherwise
                    message=['Too many input arguments!'];
                    h = msgbox( message );
                    return
            end
            
            A.omega=1./A.t_s;
            kB=1.38065e-23;
            Temp=A.Temp_K;
            a=A.a_tracer;
            t=A.t_s;
            g1=A.g1;
            %% cutoff value for low g1 values:
            g1_limit=0.1;
            g1(g1<g1_limit)=NaN; % cut
            %%
            A.msd_m2=-log(A.g1)*2/A.Q2;
            msd=A.msd_m2;
            dim=1; %dimensionality of the data.
            width=1; %width of the smoothing Gaussian weight distribution
            [msd_smooth_,df,ddf] = LSdata.logderive(t,msd,width);
            df=df'; ddf=ddf'; msd_smooth_=msd_smooth_';
            %width=20;
            %[msd_smooth_,df,ddf]=LSdata.logpolyfit_msd(t,msd,width);
            df=real(df);ddf=real(ddf);msd_smooth_=real(msd_smooth_);
            A.alpha=df;
            A.beta=ddf;
            oneone=ones(length(msd_smooth_),1);
            %size(oneone), size(df), size(ddf)
            arr1=1./gamma(oneone+df);
            arr2=1./(oneone+ddf);
            arr3=1./msd_smooth_;
            %size(arr1), size(arr2),size(arr3)
            A.G_abs=dim*kB*Temp/3/pi/a.*arr1.*arr2.*arr3;
            A.msd_smooth=msd_smooth_;
            
            % Storage and Loss moduli:
            
            A.Gp=A.G_abs.*cos(pi/2*df);
            A.Gpp=A.G_abs.*sin(pi/2*df);
            
            range1=A.Gp<0.1.*A.Gpp;
            A.Gp(range1)=NaN;
            range2=A.Gpp<0.1.*A.Gp;
            A.Gpp(range2)=NaN;
            
            
        end
        
        function report(A_)
            col1=A_.t_s; col1n=['t [s] (',A_.label,')'];
            col2=A_.g1; col2n=['g1 (',A_.label,')'];
            col3=A_.g1_raw; col3n=['raw g1 (',A_.label,')'];
            col4=A_.g1_Pu; col4n=['g1 Pusey averaged [rad/s] (',A_.label,')'];
            
            data=[col1, col2, col3, col4];
            [pathstr,name,ext] = fileparts(A_.filename);
            name=[name,'_',num2str(round(A_.Temp_C*100)/100),'C_',num2str(round(A_.angle_deg*100)/100),'deg_'];
            outfile=fullfile(A_.pathname,[name,'.txt']);
            %save(outfile_corr,'data','-ascii', '-double', '-tabs')
            header=sprintf([ col1n, '\t',col2n, '\t',col3n, '\t',col4n]);
            dlmwrite(outfile,header,'delimiter','\t');
            dlmwrite(outfile,data,'delimiter','\t','-append');
            
        end
        
        function h = g1_plot(A_,format_string)
            %g1_plot
            x_label='$t$ [s]';
            y_label='$f(q,t)$';
            h=semilogx(A_.t_s,A_.g1,format_string);
            xlabel('Lag time $t$ [s]','Interpreter','latex','FontSize',20)
            ylabel('Field correlation function $g_1(t)$','Interpreter','latex','FontSize',20)
        end
        
        function h = g1vslogt_plot(A_,format_string,axis_)
            %g1_plot
            x_label='$t$ [s]';
            y_label='$f(q,t)$';
            h=semilogx(A_.t_s,log(A_.g1),format_string);
            xlabel('Lag time $t$ [s]','Interpreter','latex','FontSize',20)
            ylabel('ln$\left[g_1(t)\right]$','Interpreter','latex','FontSize',20)
            axis(axis_) % t_upper is the upper limit of time scale to be plotted.
        end
        
        function [a_,tau_] = InitialSlopeAnalysis(A_,range)
            x=A_.t_s(range); y=A_.g1(range);
            expfit=fit(x,y,'exp1');
            figure(1)
            clf
            figure(1)
            semilogy(x,y)
            hold all
            a_=expfit.a;
            tau_=-1/expfit.b
            semilogy(x,a_*exp(-x./tau_))
            
        end
        
        function latexlabel_=latexlabel(A_)
            %check if folder basename_eval exists:
            [filepath,filename,ext]=fileparts(A_.filename);
            cd(fileparts(A_.filename))
            evalfolder=[filename,'_eval'];
            if exist(evalfolder,'dir')==7
                %waitfor(msgbox(['The folder ',evalfolder,' exists.']))
                cd(evalfolder)
            else
                %waitfor(msgbox(['The folder ',evalfolder,' does not exist.']))
            end
            if exist('latexlabel.txt','file')==2
                data=importdata('latexlabel.txt');
                latexlabel_=data{1};
            else
                latexlabel_=A_.label;
            end
        end
        
    end
    
    
    methods(Static)
        
        function [msd_smooth,dlogy,ddlogy]=logpolyfit_msd(t,msd,width)
            
            % Returns the first and second derivatives of log(y) as a function of
            % log(x), dlogy and ddlogy.
            % INPUTS:
            % t: a vector of time values (sorted from low to high values).
            % msd: a vector with the corresponding mean-square displacement values.
            % The following bounds are set on the outputs, for physical reasons:
            % dlogy<=1  : as values larger than 1 would indicate super-diffusive
            % behavior
            % dlogy>=0 :
            % width: number of points around each point, to be considered in the local
            % polynomial fit.
            % OUTPUTS:
            % y_smooth: smoothed version of y, taken from each individual fit
            % (This should be an interger value, width>1; standard: width around 3-5)
            nPtsAll=length(t);
            x=log(t);y=log(msd);
            validpts=~isnan(y);
            nanpts=isnan(y);
            x=x(validpts);y=y(validpts);
            nPts=length(y);
            % Initialize arrays
            msd_smooth_valid=ones(nPts,1);
            msd_smooth=ones(nPtsAll,1);
            dlogy_valid=ones(nPts,1);ddlogy_valid=ones(nPts,1);
            dlogy=ones(nPtsAll,1);ddlogy=ones(nPtsAll,1);
            
            
            for ii=1:nPts
                range=(max(1,(ii-width))):min(nPts,ii+width);
                % Fitting in two steps: first a linear local fit, then, keeping the
                % slope constant, a second order polynomial fit (only the first and the
                % third parameter are varied.
                %f1=fit(x(range,1),y(range,1),'poly1','Upper',[1 inf],'Lower',[0 -inf]);
                %f2=fit(x(range,1),y(range,1),'p1*(x-p2)^2+p3*x+p4','Upper',[inf f1.p1 x(ii) inf],'Lower',[-inf f1.p1 x(ii) -inf]);
                f2=fit(x(range,1),y(range,1),'poly2','Upper',[inf inf inf],'Lower',[-inf -inf -inf]);
                msd_smooth_valid(ii)=exp(f2(x(ii)));
                dlogy_valid(ii)=x(ii)*2*f2.p1+f2.p2;
                ddlogy_valid(ii)=2*f2.p1;
            end
            dlogy(validpts)=dlogy_valid; %reassign NaN's to keep vectors at the same length... :
            if nPtsAll>nPts
                dlogy(nanpts)=ones(nPtsAll-nPts,1).*NaN;
            end
            ddlogy(validpts)=ddlogy_valid;
            if nPtsAll>nPts
                ddlogy(nanpts)=ones(nPtsAll-nPts,1).*NaN;
            end
            msd_smooth(validpts)=msd_smooth_valid;
            if nPtsAll>nPts
                msd_smooth(nanpts)=ones(nPtsAll-nPts,1).*NaN;
            end
        end
        
        function [f2,df,ddf] = logderive(x,f,width)
            
            %       A special purpose routine for finding the first and
            %       second logarithmic derivatives of slowly varying,
            %       but noisy data.  It returns 'f2'-- a smoother version
            %       of 'f' and the first and second log derivative of f2.
            
            np = length(x);
            df = zeros(1,np);
            ddf = zeros(1,np);
            f2 = zeros(1,np);
            lx = log(x);
            ly = log(f);
            
            for i=1:np
                w = exp( -(lx-lx(i)).^2 / (2*width.^2) );         % a 'Gaussian'
                ww = find(w > 0.03);                           % truncate the gaussian, run faster
                %s(i)=length(ww);
                res = polyfitw(lx(ww),ly(ww),w(ww),2);
                f2(i) = exp(res(3) + res(2)*lx(i) + res(1)*(lx(i)^2));
                df(i) = res(2)+(2*res(1)*lx(i));
                ddf(i) = 2*res(1);
            end
        end
        
        function A=uiImport()
            [filen, pathn] = uigetfile( {'*.ASC'},'Pick a .ASC file to import.');
            filename=fullfile(pathn, filen);
            A=LSdata.ImportFromFile(filename);
            figure(1);
            clf;
            figure(1);
            semilogx(A.t_s,A.g1);
        end
        
        function A=ImportFromFile(filename)
            A=LSdata;
            A.filename=filename;
            [A.pathname,filen,extn]=fileparts(filename);
            
            
            
            fid=fopen(filename);
            textline=fgets(fid);
            
            if strncmp(textline,'ALV-7004',8)==true   % recognize file for "new" ALV instrument CGS-4
                file_type=2;
            elseif strncmp(textline,'ALV-5000',8)==true % recognize file for "old" ALV instrument
                file_type=3;
            else
                h = msgbox('wrong format of source file.');
                fclose(fid);
                return
            end
            
            while strncmp(textline,'"Correlation"',11)==false
                textline=fgets(fid);
                %--------------------------------------------
                % read MeasDate:
                if strncmp(textline,'Date :',6)==true
                    A.MeasDate=sscanf(textline,'Date : %s');
                end
                %--------------------------------------------
                % read MeasTime:
                if strncmp(textline,'Time :',6)==true
                    A.MeasTime=sscanf(textline,'Time : %s');
                end
                %--------------------------------------------
                % read Temp_K
                if strncmp(textline,'Temperature [K] :',17)==true
                    A.Temp_K=sscanf(textline,'Temperature [K] : %f');
                end
                A.Temp_C=A.Temp_K-273.15;
                %--------------------------------------------
                % read visc_cp
                if strncmp(textline,'Viscosity [cp]  :',17)==true
                    A.visc_cp=sscanf(textline,'Viscosity [cp]  : %f');
                end
                %--------------------------------------------
                % read n_refr
                if strncmp(textline,'Refractive Index:',17)==true
                    A.n_refr=sscanf(textline,'Refractive Index: %f');
                end
                %--------------------------------------------
                % read lambda_nm
                if strncmp(textline,'Wavelength [nm] :',17)==true
                    A.lambda_nm=sscanf(textline,'Wavelength [nm] : %f');
                end
                A.lambda_m=A.lambda_nm*1E-9;
                
                %--------------------------------------------
                % read angle_deg
                if strncmp(textline,'Angle',5)==true
                    A.angle_deg=sscanf(textline(18:length(textline)),' %f');
                end
                A.angle_rad=A.angle_deg/180*pi;
                
                %--------------------------------------------
                % read duration
                if strncmp(textline,'Duration [s]    :',17)==true
                    A.duration=sscanf(textline,'Duration [s]    : %f');
                end
                %--------------------------------------------
                % read Mode
                if strncmp(textline,'Mode            :',17)==true
                    A.Mode=sscanf(textline,'Mode            : %s');
                end
                %--------------------------------------------
                % read meanCR0
                if strncmp(textline,'MeanCR0 [kHz]   :',17)==true
                    A.meanCR0=sscanf(textline,'MeanCR0 [kHz]   : %f');
                end
                %--------------------------------------------
                % read meanCR1
                if strncmp(textline,'MeanCR1 [kHz]   :',17)==true
                    A.meanCR1=sscanf(textline,'MeanCR1 [kHz]   : %f');
                end
                CRs_=[A.meanCR0,A.meanCR1,A.meanCR2,A.meanCR3];
                A.avCR=mean(CRs_(CRs_>0));
                
            end
            
            switch file_type
                
                case 2
                    t_=zeros(560,1);ii=0;
                    corr_=zeros(1000,4);
                    while strncmp(textline,'"Count Rate"',12)==false && ii<1001
                        textline=fgets(fid); ii=ii+1;
                        x=sscanf(textline,'   %f');
                        xlength=length(x);
                        if(xlength>1)
                            t_(ii,1)=x(1);
                            corr_(ii,1:(xlength-1)) = x(2:xlength);
                        end
                    end
                    sel=t_(:,1)>0;
                    A.t_ms=t_(sel,1);A.corr=corr_(sel,:);
                    A.t_s=A.t_ms./1000;
                    % read count rate trace:
                    t_CR_=zeros(1000,1);ii=0;
                    CR_=zeros(1000,4);
                    while strncmp(textline,'Monitor Diode',13)==false
                        textline=fgets(fid);
                        ii=ii+1;
                        x=sscanf(textline,'       %f');
                        xlength=length(x);
                        if(xlength>1)
                            t_CR_(ii,1)=x(1);
                            CR_(ii,1:(xlength-1)) = x(2:xlength);
                        end
                    end
                    sel=t_CR_(:,1)>0;
                    selCR=CR_(1,:)>0;
                    A.t_CR=t_CR_(sel,1);A.CR=CR_(sel,selCR);
                    % read monitor diode:
                    A.MonitorDiode=sscanf(textline,'Monitor Diode	%f');
                    fclose(fid);
                    corr_=A.corr(:,mean(A.corr)>0);
                    A.g2min1=mean(corr_,2);         %%%%%%%%% previous error location, fault with mean of transpose
                    A.g2=A.g2min1+1;
                    % calculate g1
                    A.g2min1(A.g2min1<0)=NaN; %get rid of negative entries
                    A.g1_raw=sqrt(A.g2min1);
                    % calculate g1_scaled:
%                     IC=mean(A.g1_raw(1:20)');
%                     sel_isnumber=~isnan(A.g1_raw);
%                     initial_points=A.g1_raw(sel_isnumber);
%                     initial_points
%                     initial_points=initial_points(1:20);
%                     IC=mean(initial_points);
                    IC=mean(A.g1_raw(1:20));
                    A.g1_scaled=A.g1_raw./IC;
                    A.g1=A.g1_scaled;
                    
                    
                case 3
                    % read correlation functions:
                    t_=zeros(1000,1);ii=0;
                    corr_=zeros(1000,1);
                    while strncmp(textline,'"Count Rate"',12)==false && ii<1001
                        textline=fgets(fid); ii=ii+1;
                        x=sscanf(textline,'   %f');
                        if(length(x)>1)
                            t_(ii,1)=x(1);
                            corr_(ii,1) = x(2);
                        end
                    end
                    sel=t_(:,1)>0;
                    A.t_ms=t_(sel,1);A.corr=corr_(sel,1);
                    A.t_s=A.t_ms./1000;
                    % read count rate trace:
                    t_CR_=zeros(1000,1);ii=0;
                    CR_=zeros(1000,4);
                    while strncmp(textline,'Monitor Diode',13)==false
                        textline=fgets(fid); ii=ii+1;
                        x=sscanf(textline,'   %f');
                        
                        if(length(x)>1)
                            t_CR_(ii,1)=x(1);
                            CR_(ii,2:(length(x))) = x(2:(length(x)));
                        end
                    end
                    sel=CR_(:,1)>0;
                    A.t_CR=t_CR_(sel,1);A.CR=CR_(sel,1:4);
                    % read monitor diode:
                    A.MonitorDiode=sscanf(textline,'Monitor Diode	%f');
                    fclose(fid);
                    A.g2min1=A.corr;
                    A.g2min1=A.corr;
                    A.g2=A.g2min1+1;
                    % calculate g1
                    A.g2min1(A.g2min1<0)=NaN; %get rid of negative entries
                    A.g1_raw=sqrt(A.g2min1);
                    % calculate g1_scaled:
                    IC=mean(A.g1_raw(1:20)');
                    A.g1_scaled=A.g1_raw./IC;
            end
            
            
            % calculate Q:
            A.Q=4*pi*A.n_refr/A.lambda_m*sin(A.angle_rad/2);
            A.Q2=A.Q^2;
            
            % set standard values for output_string and label:
            [pathn,filen,ext] = fileparts(A.filename);
            A.output_string=filen; % cut of file extension
            if ~isempty(A.Q)
                A.label=['T = ',num2str(round(A.Temp_C*100)./100),' [C], angle = ',num2str(round(A.angle_deg*100)./100),' deg, 1/Q = ',num2str(round(1./A.Q*1e9*10)./10),' nm'];
            end
            
            %Check if Pusey folder exists, if yes: perform Pusey averaging
            puseyfolder=[filen,'_Pu'];
            puseyfolder=fullfile(pathn,puseyfolder);
            if exist(puseyfolder,'dir')==7
                puseyfiles=dir([puseyfolder,'/*.ASC']);
                pu1=fullfile(puseyfolder,puseyfiles(1).name);
                pu2=fullfile(puseyfolder,puseyfiles(1).name);
                pu3=fullfile(puseyfolder,puseyfiles(1).name);
                if size(puseyfiles,1)==3
                    A=A.PuseyAverage(pu1, pu2, pu3,1:20);
                    %['Pusey averaged the file ',A.filename]
                end
            else
                %waitfor(msgbox('Pusey folder does not exist'));
                A.g1=A.g1_scaled;
            end
            
            
        end  % end of function ImportFromFile
        
        function A_array=EvalAllInFolder(theFolder)
            close all
            if exist(theFolder,'dir')==7
                ASCfiles=dir([theFolder,'/*.ASC']);
                nFiles=size(ASCfiles,1);
                if nFiles>0
                    for ii=1:nFiles
                        theFile=fullfile(theFolder,ASCfiles(ii).name);
                        A_array(ii)=LSdata.ImportFromFile(theFile);
                    end
                else
                    waitfor(msgbox('No files..'));
                    A_array=LSdata;
                end
            end
            
            
            % Extract and output data about this set of measurements:
            range=1:nFiles;
            LL=length(A_array(1).t_s);
            
            g1=zeros(LL,nFiles);
            t_s=zeros(LL,nFiles);
            msd_m2=zeros(LL,nFiles);
            msd_smooth=zeros(LL,nFiles);
            omega=zeros(LL,nFiles);
            Gs=zeros(LL,nFiles);
            Gp=zeros(LL,nFiles);
            Gpp=zeros(LL,nFiles);
            
            g1_plot=figure; title('Dynamic structure factors');
            msd_m2_plot=figure; title('Mean-square displacements');
            G_abs_plot=figure; title('Magnitude of the complex modulus |G*|');
            
            % Get the name of the parent folder (name of the measurement
            % series)
            [parent_path, parentfoldername, ~] = fileparts(cd);
            %evalfolder=fullfile(parent_path,parentfoldername,[A_array(ii).filename,'_eval']);
            
            
            
            % Tracer particle size:
            
            if exist('tracer_radius.txt')==2
                doMicroRheo=true;
                a=load('tracer_radius.txt','-ascii');
                for ii=1:nFiles
                    A_array(ii).a_tracer=a;
                end
            else
                prompt={'If microrheology analysis should be performed, enter the tracer particle size [in meters] and press OK. Otherwise, press Cancel.'};
                name='Microrheology analysis'; numlines=1;
                defaultanswer={num2str(A_array(1).a_tracer)};
                answer=inputdlg(prompt,name,numlines,defaultanswer);
                if isempty(answer)
                    doMicroRheo=false;
                else
                    doMicroRheo=true;
                    a=str2double(answer{1});
                    save('tracer_radius.txt','a','-ascii');
                    for ii=1:nFiles
                        A_array(ii).a_tracer=a;
                    end
                end
            end
            
            
            for ii=1:nFiles
                
                % Do microrheology
                if doMicroRheo==true
                    A_array(ii)=A_array(ii).calc_G(a);
                    %
                    msd_m2=A_array(ii).msd_m2;
                    msd_smooth=A_array(ii).msd_smooth;
                    omega=A_array(ii).omega;
                    Gs=A_array(ii).Gs;
                    Gp=A_array(ii).Gp;
                    Gpp=A_array(ii).Gpp;
                    G_abs=A_array(ii).G_abs;
                    
                    %plot all msd's:
                    figure(msd_m2_plot)
                    loglog(A_array(ii).t_s,msd_m2,LSdata.LSlinestyle(ii));
                    hold on
                    
                    %plot all G_abs's
                    figure(G_abs_plot)
                    loglog(omega,G_abs,LSdata.LSlinestyle(ii));
                    hold on
                end
                
                
                legend_{ii}=A_array(ii).label; % curve legend for all plots
                
                %plot all g1's
                figure(g1_plot)
                semilogx(A_array(ii).t_s,A_array(ii).g1,LSdata.LSlinestyle(ii));
                
                hold on
                
                
            end
            
            
            
            % Save array of objects as mat file:
            outfile=[parentfoldername,'_LS_data_series.mat'];
            fulloutfile=fullfile(theFolder,outfile);
            save(fulloutfile,'A_array');
            
            % evalfolder to save output for individual files:
            for ii=1:nFiles
                [fpath,fname]=fileparts(A_array(ii).filename);
                evalfolder{ii}=[fname,'_eval'];
                if not(exist(evalfolder{ii},'dir')==7) % if eval folder does not exist, create it.
                    mkdir(evalfolder{ii});
                end
                
            end
            
            % save data for individual file to .mat file
            for ii=1:nFiles
                A_=A_array(ii);
                outputfile=fullfile(evalfolder{ii},[A_array(ii).output_string,'.mat']);
                save(outputfile,'A_');
            end
            
            %save g1 series plot
            figure(g1_plot)
            g1_graph_title_=parentfoldername;
            g1_xlabel_='$t$ [s]';
            g1_ylabel_='$f(q,t)$';
            axis([1e-8 10 0 1.05]);
            legend(legend_);
            LSdata.nicify_plot(g1_plot,g1_graph_title_,g1_xlabel_,g1_ylabel_);
            set(gcf,'PaperOrientation','landscape');
            set(gcf,'PaperPositionMode','auto');
            outfile=[parentfoldername,'_g1_vs_t_all','.pdf'];
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            
            %save msd_m2 series plot
            figure(msd_m2_plot)
            msd_m2_graph_title_=parentfoldername;
            msd_m2_xlabel_='$t$ [s]';
            msd_m2_ylabel_='MSD$(t)$ [m$^2$]';
            axis([1e-5 10 1e-20 1e-14]);
            legend(legend_);
            LSdata.nicify_plot(msd_m2_plot,msd_m2_graph_title_,msd_m2_xlabel_,msd_m2_ylabel_);
            set(gcf,'PaperOrientation','landscape');
            set(gcf,'PaperPositionMode','auto');
            outfile=[parentfoldername,'_msd_m2_vs_t_all','.pdf'];
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            
            %save G_abs series plot
            figure(G_abs_plot)
            G_abs_graph_title_=parentfoldername;
            G_abs_xlabel_='Frequency $\omega$ [s$^-1$]';
            G_abs_ylabel_='Modulus $|G^*(\omega)|$';
            axis([1e-1 1e6 1e-2 1e5]);
            legend(legend_);
            LSdata.nicify_plot(G_abs_plot,G_abs_graph_title_,G_abs_xlabel_,G_abs_ylabel_);
            set(gcf,'PaperOrientation','landscape');
            set(gcf,'PaperPositionMode','auto');
            outfile=[parentfoldername,'_G_abs_vs_t_all','.pdf'];
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            
            %outputfilename=[graph_title,'.jpg'];
            %saveas(gcf,outputfilename);
        end
        
        function h=nicify_plot(h,graph_title_,xlabel_,ylabel_)
            
            figure(h)
            
            set(gca,'LineWidth',1);
            set(gca,'FontSize',14);
            xlabel(xlabel_,'Fontsize',24,'Interpreter','latex');
            ylabel(ylabel_,'Fontsize',24,'Interpreter','latex');
            title(graph_title_,'Interpreter','latex');
            set(gcf, 'PaperUnits', 'centimeters');
            set(gcf, 'Units', 'centimeters');
            set(gcf, 'Position', [2 2 17 17]);
            set(gca, 'Units', 'centimeters');
            legend('Location','Best');
            set(gca, 'Position', [3 3 12 12]);
            legend(gca,'show');
            legend(gca,'boxoff');
            LEGH = legend;
            set(LEGH,'FontSize',10);
            set(LEGH,'Interpreter','latex');
            set(get(gca,'Children'),'linewidth',1); % This sets the linewidth for all plots. (plots are 'Children' of the current axis.
            set(gcf,'PaperOrientation','portrait');
            set(gcf,'PaperPositionMode','auto');
        end
        
        function [line,marker,color]=LSLineMarkerColor(ii)
            
            colors = {'k', 'b', 'g', 'r', 'm'};
            markers = {'o', 'x', '+', '*', 's', 'd', 'v', '^', '<', '>', 'p'};
            lines = {'-', '-.', '--'};
            line = lines{mod(ii-1, length(lines))+1};
            marker = markers{mod(ii-1, length(markers))+1};
            color = colors{mod(ii-1, length(colors))+1};
        end
        
        function linestyle_=LSlinestyle(ii)
            [line,marker,color]=LSdata.LSLineMarkerColor(ii);
            linestyle_=[line,color];
        end
        
        function linestyle_=LSsymbolstyle(ii)
            [line,marker,color]=LSdata.LSLineMarkerColor(ii);
            linestyle_=[marker,color];
        end
        
        function obj = LSdata(n) %primitive class constructor
            if nargin > 0
                if n>0
                    for ii=1:n
                        obj(ii)=LSdata;
                    end
                end
            end
        end
        
    end
    
end
