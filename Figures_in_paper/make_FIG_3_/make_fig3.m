clear all
close all
%%Concentrations and folder paths to data from dilutions A to D

% Dilution A : 50% of dilution1, [500 mL dil1 + 500 mL H2O] => Expected l*:  ~150 �m
% Dilution A2: 33% of dilution1 [330 mL dil1 + 670 mL H2O] => Expected l*:  ~227 �m
% Dilution B : 25% of dilution1, [250 mL dil1 + 750 mL H2O]  => Expected l*:  ~300 �m
% Dilution B2: 18% of dilution1 [180 mL dil1 + 820 mL H2O] => Expected l*:  ~417 �m
% Dilution C : 12.5 % of dilution1, [125 mL dil1 + 875 mL H2O]----- this is the current dilution 2 => Expected l*: ~600 �m
% Dilution C2: 9% of dilution1 [90 mL dil1 + 910 mL H2O] => Expected l*:  ~833 �m
% Dilution D : 6.25%% of dilution1, [62.5 mL dil1 + 937.5 mL H2O]  Expected l*:  ~1200 �m (here the diffusion approximation should only hold for small detection angles, i.e. distance between entry and exit points of photons ~ 10 mm..)

conc=[50;33;25;18;12.5;9;6.26]./100*0.05; %particle volume fractions (absolute)
sample_name={'Dilution A','Dilution A2','Dilution B','Dilution B2','Dilution C',...
             'Dilution C2','Dilution D'};
data_path={...
    './data/diluA/VH/',...
    './data/diluB/VH/',...
    './data/diluA2/VH/',...
    './data/diluB2/VH/',...
    './data/diluC/VH/',...
    './data/diluC2/VH/',...
    './data/diluD/VH/'};
basename={...
    'DiluA_VH00',...
    'DiluB_VH00',...
    'DiluA2_VH00',...
    'DiluB2_VH00',...
    'DiluC_VH00',...
    'DiluC2_VH00',...
    'DiluD_VH00'};



%%



%%

filenr=7; %nr 7: 90 degree angle
pathname=data_path{5}; % nr 5: concentration phi=0.63%
filename=[basename{5},num2str_2digits(filenr),'_0001.ASC'];
%filename='ALV_DWS_dil2_0008.ASC';

fullname=[pathname,filename];
A=DLSDWSdata.ImportFromFile(fullname);
A.angle_deg

endstring='_0001.ASC';

cc=1;aa=1;
ASCfile = inline('[data_path{cc},basename{cc},num2str_2digits(aa),endstring]','cc','aa','data_path','basename','endstring')

%ASCfile = inline('[data_path{cc},basename{cc},num2str_2digits(aa),endstring]','cc','aa')
%ASCfile=@(cc,aa) fullname(data_path{cc},[basename{cc},num2str_2digits(aa),'_0001.ASC'])
ASCfile(1,2,data_path,basename,endstring)


%%
global t_exp
global g1_exp
global R
global angle
global lambda

for cc=1:7 %loop over 7 different concentrations
    for aa=1:13 %loop over 13 different angles
        fullname=ASCfile(cc,aa,data_path,basename,endstring);
        A=DLSDWSdata.ImportFromFile(fullname);

        % find optimal lstar via least squares fit:

        
        t_exp=A.t_s;
        g1_exp=A.g1_scaled;
        R=0.0045;
        angle=A.angle_deg;
        angle_(aa)=angle;

        lambda=A.lambda_m;
        
        range=(g1_exp>0.1 & g1_exp<0.95);
        t_exp=A.t_s(range);
        g1_exp=A.g1_scaled(range);
        
        lstar=500e-6; %starting value for the fit
        [s,Ps] = P_s_from_master_curve(R, lstar, angle);
        
        % Find the optimal lstar value via a least squares fit:
        x0=1200e-6;
        x = fminsearch(@f_to_minimize, x0);
        
        lstar_opt(cc,aa)=x;
        [s,Ps] = P_s_from_master_curve(R, x, angle);
        s_av(cc,aa)=sum(s.*Ps)./sum(Ps);
        
%         To pass parameters using anonymous functions:
%         
%         Write a file containing the following code:
%         function y = parameterfun(x,a,b,c)
%         y = (a - b*x(1)^2 + x(1)^4/3)*x(1)^2 + x(1)*x(2) + ...
%             (-c + c*x(2)^2)*x(2)^2;
%         Assign values to the parameters and define a function handle f to an anonymous function by entering the following commands at the MATLAB� prompt:
%         a = 4; b = 2.1; c = 4; % Assign parameter values
%         x0 = [0.5,0.5];
%         f = @(x)parameterfun(x,a,b,c)
        

        %fun = @(lstar)g1_fun(lstar,R,angle);
        
        %x = lsqcurvefit(g1_fun,x0,t_exp,g1_exp)
        
    end
end
%%
figure()
for aa=1:13
    sel=s_av(1:7,aa)
    loglog(conc,lstar_opt(1:7,aa),'o-')
    hold all
end

%%

%% sub-plot 7:
mainh=figure(33)
h_sp7=subplot(3,3,7)
for cc=1:7
semilogy(angle_(1:13),lstar_opt(cc,1:13),'o-')
hold all
end
xlabel_='Detection angle $\theta$ [$^\circ$]';
ylabel_='Fitted $l^\star$ [m]';
axis([25 155 1e-4 1.4e-3])
legend_={['$\phi=',num2str(conc(1)*100),'\%$)'],...
        ['$\phi=',num2str(conc(2)*100),'\%$)'],...
        ['$\phi=',num2str(conc(3)*100),'\%$)'],...
        ['$\phi=',num2str(conc(4)*100),'\%$)'],...
        ['$\phi=',num2str(conc(5)*100),'\%$)'],...
        ['$\phi=',num2str(conc(6)*100),'\%$)'],...
        ['$\phi=',num2str(conc(7)*100),'\%$)']};
legend(legend_);
nicify_subplot(3,3,7,xlabel_,ylabel_)

%% sub-plot 8:

figure(mainh)
h_sp8=subplot(3,3,8)
for aa=1:13
    sel=s_av(1:7,aa)
    loglog(conc,lstar_opt(1:7,aa),'o-')
    hold all
    legend_{aa}=['$\theta=',num2str(angle_(aa)),'^\circ$'];
end
axis([2e-3 1e-1 1e-4 2e-3])
legend(legend_);
xlabel_='Volume fraction $\phi$';
ylabel_='Fitted $l^\star$ [m]';
nicify_subplot(3,3,8,xlabel_,ylabel_)


%% sub-plot 1:

cc=5;
aa=3;
A=DLSDWSdata.ImportFromFile(ASCfile(cc,aa,data_path,basename,endstring));

figure(mainh)
h_sp1=subplot(3,3,1)

semilogx(A.t_s,A.g1_scaled,'o')
hold on

lstar=lstar_opt(cc,aa);
angle=angle_(aa);
R=0.0045;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);
g1_sim=g1_from_Ps(532e-9,s,Ps,lstar,A.t_s);
semilogx(A.t_s,g1_sim,'r-')

axis([1e-7 1e-3 0 1.05]);
xlabel_='$t$ [s]';
ylabel_='$g_1(t)$';
legend_={['exp. ($\phi=',num2str(conc(cc)*100),'\%$)'],['sim. ($l^\star=',num2str(round(lstar*1e6)),'\mu$m)']};
legend(legend_);
nicify_subplot(3,3,1,xlabel_,ylabel_)



%% sub-plot 2:


aa=7;
A=DLSDWSdata.ImportFromFile(ASCfile(cc,aa,data_path,basename,endstring));

figure(mainh)
h_sp1=subplot(3,3,2)

semilogx(A.t_s,A.g1_scaled,'o')
hold on

lstar=lstar_opt(cc,aa);
angle=angle_(aa);
R=0.0045;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);
g1_sim=g1_from_Ps(532e-9,s,Ps,lstar,A.t_s);
semilogx(A.t_s,g1_sim,'r-')

axis([1e-7 1e-3 0 1.05]);
xlabel_='$t$ [s]';
ylabel_='$g_1(t)$';
legend_={['exp. ($\phi=',num2str(conc(cc)*100),'\%$)'],['sim. ($l^\star=',num2str(round(lstar*1e6)),'\mu$m)']};
legend(legend_);
nicify_subplot(3,3,2,xlabel_,ylabel_)



%% sub-plot 3:


aa=11;
A=DLSDWSdata.ImportFromFile(ASCfile(cc,aa,data_path,basename,endstring));

figure(mainh)
h_sp1=subplot(3,3,3);

semilogx(A.t_s,A.g1_scaled,'o')
hold on

lstar=lstar_opt(cc,aa);
angle=angle_(aa);
R=0.0045;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);
g1_sim=g1_from_Ps(532e-9,s,Ps,lstar,A.t_s);
semilogx(A.t_s,g1_sim,'r-')

axis([1e-7 1e-3 0 1.05]);
xlabel_='$t$ [s]';
ylabel_='$g_1(t)$';
legend_={['exp. ($\phi=',num2str(conc(cc)*100),'\%$)'],['sim. ($l^\star=',num2str(round(lstar*1e6)),'\mu$m)']};
legend(legend_);
nicify_subplot(3,3,3,xlabel_,ylabel_)

%% sub-plot 4:


aa=3;
A=DLSDWSdata.ImportFromFile(ASCfile(cc,aa,data_path,basename,endstring));

figure(mainh)
h_sp4=subplot(3,3,4)

plot(A.t_s,log(A.g1_scaled),'o')
hold on

lstar=lstar_opt(cc,aa);
angle=angle_(aa);
R=0.0045;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);
g1_sim=g1_from_Ps(532e-9,s,Ps,lstar,A.t_s);
plot(A.t_s,log(g1_sim),'r-')

axis([0 2.4e-4 -3 0]);
xlabel_='$t$ [s]';
ylabel_='$\log\ g_1(t)$';
legend_={['exp. ($\phi=',num2str(conc(cc)*100),'\%$)'],['sim. ($l^\star=',num2str(round(lstar*1e6)),'\mu$m)']};
legend(legend_);
nicify_subplot(3,3,4,xlabel_,ylabel_)


%% sub-plot 5:


aa=7;
A=DLSDWSdata.ImportFromFile(ASCfile(cc,aa,data_path,basename,endstring));

figure(mainh)
h_sp4=subplot(3,3,5)

plot(A.t_s,log(A.g1_scaled),'o')
hold on

lstar=lstar_opt(cc,aa);
angle=angle_(aa);
R=0.0045;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);
g1_sim=g1_from_Ps(532e-9,s,Ps,lstar,A.t_s);
plot(A.t_s,log(g1_sim),'r-')

axis([0 3.5e-4 -3 0]);
xlabel_='$t$ [s]';
ylabel_='$\log\ g_1(t)$';
legend_={['exp. ($\phi=',num2str(conc(cc)*100),'\%$)'],['sim. ($l^\star=',num2str(round(lstar*1e6)),'\mu$m)']};
legend(legend_);
nicify_subplot(3,3,5,xlabel_,ylabel_)

%% sub-plot 6:


aa=11;
A=DLSDWSdata.ImportFromFile(ASCfile(cc,aa,data_path,basename,endstring));

figure(mainh)
h_sp4=subplot(3,3,6)

plot(A.t_s,log(A.g1_scaled),'o')
hold on

lstar=lstar_opt(cc,aa);
angle=angle_(aa);
R=0.0045;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);
g1_sim=g1_from_Ps(532e-9,s,Ps,lstar,A.t_s);
plot(A.t_s,log(g1_sim),'r-')

axis([0 7e-4 -3 0]);
xlabel_='$t$ [s]';
ylabel_='$\log\ g_1(t)$';
legend_={['exp. ($\phi=',num2str(conc(cc)*100),'\%$)'],['sim. ($l^\star=',num2str(round(lstar*1e6)),'\mu$m)']};
legend(legend_);
nicify_subplot(3,3,6,xlabel_,ylabel_)

%% export optimized path length into mat file
save('lstar_opt.mat','lstar_opt')

%% export main figure
% figure(mainh)
% export_fig('fig3.pdf','-transparent')