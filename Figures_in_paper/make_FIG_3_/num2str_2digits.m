function s=num2str_2digits(n)
if n<10
    s=['0',sprintf('%i',n)];
elseif n<100
    s=sprintf('%i',n);
else
    s='__'
end