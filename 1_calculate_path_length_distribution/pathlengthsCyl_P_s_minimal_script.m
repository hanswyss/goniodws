% Iterate over l* values spaced by factors of two.

N_loops=500; %Each loop takes about 36 s to calculate using both cores of macbook air 2013

N_angles=180;
x=1:N_angles;

for n=1:N_loops
    tic
    for ii=1:6
        A=0;
        lstar(ii)=0.005*2^ii;
        parfor i=1:10
            A=A+pathlengthsCyl_P_s([lstar(ii),N_angles,100000]);
        end
        A=A./10;
        
        basename=['P_s_',num2str(lstar(ii)),'_',num2str(N_angles)];
        update_output_file(A,basename,1e6);
    end
    ['Iteration ',num2str(n),' of ',num2str(N_loops)]
    toc
end
        
