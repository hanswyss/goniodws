#include "mex.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/*
 *
 * Function that calculates the pathlength distribution for diffusion of photons
 * through a cylinder
 *
 */

void pathlengthsCyl(double y[], double x[], size_t mrows, size_t ncols)
{
    int ii,m,n,step,anglebin,Nbins,N_iter,index1,index2,index3;
    double xx,yy,zz,dxx,dyy,dzz,lstar,LL,pi,deltaz,weight,dist;
    
    lstar=x[0];
    Nbins=x[1];
    N_iter=x[2];
    pi=3.14159265359;
    
    //srand( (unsigned)time( NULL ) );
    srand ( rand() ^ time(NULL) );
    
    for(m=0;m<mrows;m++)
    {
        for(n=0;n<ncols;n++)
        {
           index1=(m % mrows)+mrows*n;
           y[index1]=0;
           
        }
    }
    
    for(n=0;n<ncols;n++)
    {
        dist=sqrt( (-1-cos(pi/Nbins*n))*(-1-cos(pi/Nbins*n))+sin(pi/Nbins*n)*sin(pi/Nbins*n) );
        index1=2+mrows*n;
        y[index1]=ceil(dist*dist/lstar/lstar/100); //write column 2: the width of each bin (set as one hundreds of the expected average number of scattering events.)
    }
    
    
    for(ii=0;ii<N_iter;ii++)
    {
        xx=-1+lstar;
        yy=0;zz=0;
        step=1;
        while ((xx*xx+yy*yy)<1)
        {
            
            // Random unit vector of length lstar:
            dxx=2;dyy=2;dzz=2;
            while (dxx*dxx+dyy*dyy+dzz*dzz>1) // make sure (dxx,dyy,dzz) is a vector of random direction.
            {
                dxx=2*(double)(rand()) / (double)(RAND_MAX)-1;
                dyy=2*(double)(rand()) / (double)(RAND_MAX)-1;
                dzz=2*(double)(rand()) / (double)(RAND_MAX)-1;
            }
            LL=sqrt(dxx*dxx+dyy*dyy+dzz*dzz);
            dxx=dxx/LL*lstar;dyy=dyy/LL*lstar;dzz=dzz/LL*lstar;
            // Propagate by a step lstar:
            xx=xx+dxx;yy=yy+dyy;zz=zz+dzz;
            step++;
        }
        LL=sqrt(xx*xx+yy*yy);
        xx=xx/LL;yy=yy/LL;zz=zz/LL;
        anglebin=ceil(acos(xx)*(Nbins-1)/pi);
        
        index1=mrows*anglebin;
        deltaz=pi/2/Nbins;
        weight=erf(3*deltaz/lstar/sqrt(step)); //account for probability in z-direction to hit area around zz=0
            if(weight==0) mexPrintf("erf: %f\n",weight);
            y[index1]=y[index1]+weight;
            // Update average number of steps and corresponding added weights:
            index2=(1 % mrows)+mrows*anglebin;
            y[index2]=y[index2]+step*weight;
            index3=floor(step/y[anglebin*mrows+2])+3+mrows*anglebin;
            if(floor(step/y[anglebin*mrows+2])+3<mrows)
            {
                y[index3]=y[index3]+weight; // add value to bin
            }

    }
    
    for(anglebin=0;anglebin<Nbins;anglebin++)
        
    {
        index1=mrows*anglebin;
        index2=(1 % mrows)+mrows*anglebin;
        if(y[index1]==0)
        {
          //mexPrintf("y[index1] is zero. index1:%i y[index1]: %f, y[index2]: %f\n",index1,y[index1],y[index2]);
        }
        if(y[index2]>0)
        {
        y[index2]=y[index2]/y[index1];
        }
    }

}





void mexFunction( int nlhs, mxArray *plhs[],
        int nrhs, const mxArray *prhs[] )
{
    double *x,*y;
    double lstar;
    int Nbins;
    size_t mrows,ncols,mrows_out,ncols_out;
    
    /* Check for proper number of arguments. */
    if(nrhs!=1) {
        mexErrMsgIdAndTxt( "MATLAB:timestwo:invalidNumInputs",
                "One input required.");
    } else if(nlhs>1) {
        mexErrMsgIdAndTxt( "MATLAB:timestwo:maxlhs",
                "Too many output arguments.");
    }
    
    /* The input must be noncomplex doubles.*/
    mrows = mxGetM(prhs[0]);
    ncols = mxGetN(prhs[0]);
    if( !mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]) ) {
        mexErrMsgIdAndTxt( "MATLAB:timestwo:inputNotRealScalarDouble",
                "Input must be noncomplex doubles.");
    }
    
    /* Assign pointer for the input matrix.*/
    x = mxGetPr(prhs[0]);
    lstar=x[0];
    //x[1]=floor(Nbins);
    Nbins=x[1];
    
    /* Create matrix for the return argument. */
    
    mrows_out=303; 
// row 0: average intensity for this angle bin; 
// row 1: average path length for this angle bin. 
// row 2: bin width for this angle bin. 
// rows 3-302: path length distribution for this angle bin.
    ncols_out=Nbins;  /* one column for each angle segment. The columns give a counter for each         */
    plhs[0] = mxCreateDoubleMatrix((mwSize)mrows_out, (mwSize)ncols_out, mxREAL);
    
    /* Assign pointer for the output matrix. */
    
    y = mxGetPr(plhs[0]);
    
    /* Call the pathlengths subroutine. */
    pathlengthsCyl(y,x,mrows_out,ncols_out);
}
