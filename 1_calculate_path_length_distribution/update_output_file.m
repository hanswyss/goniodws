function update_output_file(A,basename,N_photons)
    % function to update output file "basename".mat for results of
    % path length calculation.
    % Also write to a log file "basename"_log.txt the details of
    % information contained in "basename".mat
    global N_photons_file;
    global A_file;
    
    
    % If yes, first read information contained in the file, combine
    % information from input A and from 
    
    % 3. If no, write information to file.
    
    filename=[basename,'.mat'];
    % 1. Check if file exists:
    if exist(filename)==2
        % If yes, first read information contained in the file, combine
        % information from input A and from information contained in the file.
        load(filename)
        A_file=(N_photons_file.*A_file + N_photons.*A)./(N_photons+N_photons_file);
        N_photons_file=N_photons_file+N_photons;
        save(filename,'A_file','N_photons_file');
    else
        % If no, write information to file. 
        A_file=A;
        N_photons_file=N_photons;
        save(filename,'A_file','N_photons_file');
    end
end

