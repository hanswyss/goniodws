R=5e-3; %Radius of cylindrical cell: 5 mm
anglebin=135; %angle bin (degrees)


for ii=1:5
    lstar(ii)=0.01*2^(ii-1)
end
global A_file;
clear legend_
figure(1)
clf
figure(1)

figure(2)
clf
figure(2)

for ii=1:5
    lstar_over_R=lstar(ii);
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(180),'.mat'];
    if exist(filename)==2
        load(filename);
        
        binwidth=A_file(3,anglebin);
        average_s=R*A_file(2,anglebin);
        I_tot=A_file(1,anglebin);
        Ps=A_file(4:303,anglebin)./I_tot./binwidth./R;
        s=((1:300).*binwidth*R)';
        legend_{ii}=['$l^{\star}/R$ = ',num2str(lstar_over_R)];
        
    else
        Ps=0; s=0;
        ['File ',filename,' does not exist.']
    end
    figure(1)
    loglog(s,Ps)
    hold all
    
    
    %Figure 2: Normalized by average path length:
    
    figure(2)
    loglog(s./average_s,Ps.*average_s)
    hold all
    
    figure(3)
    loglog(lstar_over_R,average_s,'ro');
    hold on
    loglog(lstar_over_R,(lstar(1)/lstar_over_R)^2,'ro');
end
%%
figure(1)
legend(legend_)
LSdata.nicify_plot(gcf,['$P(s)$ vs. $s$ for different values of $l^{\star}/R$  ($\theta=$',num2str(anglebin),'$^\circ$).'],'$s$ [m]','$P(s)$');
axis([1e-2 1e3 1e-3 10]);
saveas(gcf, ['Ps_vs_s_',num2str(anglebin),'deg'], 'pdf')
saveas(gcf, ['Ps_vs_s_',num2str(anglebin),'deg'], 'fig')

%%
figure(2)
legend(legend_)
LSdata.nicify_plot(gcf,['$P(s) \cdot \tilde{s}$ vs. $s/\tilde{s}$ for different values of $l^{\star}/R$  ($\theta=$',num2str(anglebin),'$^\circ$).'],'$s / \tilde{s}$ [m]','$P(s) \cdot \tilde{s}$');
axis([1e-2 1e3 1e-3 10]);
saveas(gcf, ['Ps_vs_s_mastercurve',num2str(anglebin),'deg'], 'pdf')
saveas(gcf, ['Ps_vs_s_mastercurve',num2str(anglebin),'deg'], 'fig')



