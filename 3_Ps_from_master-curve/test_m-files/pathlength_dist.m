function P_s = pathlength_dist(R, lstar, N_anglebins, anglebin, s)
    global N_photons_file;
    global A_file;
    filename=['P_s_',num2str(lstar),'_',num2str(N_anglebins),'.mat'];
    if exist(filename)==2
        load(filename);
        
        binwidth=A_file(3,anglebin);
        I_tot=A_file(1,anglebin);
        P_s_raw=A_file(4:303,anglebin)./I_tot./binwidth./R;
        s_raw=((1:300).*binwidth*R)';
        P_s = interp1(s_raw,P_s_raw,s,'linear'); % cubic spline interpolation for P(s) based P_s_raw(s_raw). 
    
    else
        P_s=0;
        ['File ',filename,' does not exist.']
    end
end