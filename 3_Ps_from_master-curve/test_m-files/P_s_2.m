function [s,Ps] = P_s_2(R, lstar_over_R, N_anglebins, anglebin)
    global N_photons_file;
    global A_file;
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(N_anglebins),'.mat'];
    lstar_=R*lstar_over_R;
    if exist(filename)==2
        load(filename);
        
        binwidth=A_file(3,anglebin);
        avN=A_file(2,anglebin)
        I_tot=A_file(1,anglebin);
        Ps=A_file(4:303,anglebin)./I_tot./binwidth./lstar_;
        s=((1:300).*binwidth*lstar_)';
        
    else
        P_s=0; s=0;
        ['File ',filename,' does not exist.']
    end
end