R=4.5e-3; %Radius of cylindrical cell: 5 mm
anglebin=10; %angle bin (degrees)

close all
for ii=1:5
    lstar(ii)=0.01*2^(ii-1)
end
global A_file;
clear legend_
figure(1)
clf
figure(1)

figure(2)
clf
figure(2)

for ii=1:5
    lstar_over_R=lstar(ii);
    lstar_=R*lstar_over_R;
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(180),'.mat'];
    if exist(filename)==2
        load(filename);
        
        binwidth=A_file(3,anglebin);
        average_s=lstar_*A_file(2,anglebin);
        average_s_(ii)=average_s;
        I_tot=A_file(1,anglebin);
        Ps=A_file(4:303,anglebin)./I_tot./binwidth./lstar_;
        s=((1:300).*binwidth.*lstar_)';
        legend_{ii}=['$l^{\star}/R$ = ',num2str(lstar_over_R)];
        
    else
        Ps=0; s=0;
        ['File ',filename,' does not exist.']
    end
    figure(1)
    loglog(s,smooth(Ps,3))
    hold all
    
    
    %Figure 2: Normalized by average path length:
    
    figure(2)
    loglog(s./average_s,smooth(Ps.*average_s,3))
    hold all
end
%%
figure(1)
legend(legend_)
LSdata.nicify_plot(gcf,['$P(s)$ vs. $s$ for different values of $l^{\star}/R$  ($\theta=$',num2str(anglebin),'$^\circ$).'],'$s$ [m]','$P(s)$');
axis([4e-3 1 1e-1 100]);
saveas(gcf, ['Ps_vs_s_',num2str(anglebin),'deg'], 'pdf')
saveas(gcf, ['Ps_vs_s_',num2str(anglebin),'deg'], 'fig')

%%
figure(2)
legend(legend_)
LSdata.nicify_plot(gcf,['$P(s) \cdot \tilde{s}$ vs. $s/\tilde{s}$ for different values of $l^{\star}/R$  ($\theta=$',num2str(anglebin),'$^\circ$).'],'$s / \tilde{s}$ [m]','$P(s) \cdot \tilde{s}$');
axis([1e-1 10 1e-3 10]);
saveas(gcf, ['Ps_vs_s_mastercurve',num2str(anglebin),'deg'], 'pdf')
saveas(gcf, ['Ps_vs_s_mastercurve',num2str(anglebin),'deg'], 'fig')

%%  Average number of scattering events vs. l*/R
figure(3)
loglog(lstar,average_s_./(lstar.*R),'o')

%% Average path length as a function of the distance D between entry and exit points
% (for fixed l* value; choose l*/R=0.08)
colors='brgmkcybrgmkcybrgmkcy';
symbols='od+xsod+xsod+xsod+xsod+xs';
%format_string=[colors(format_nr),symbols(format_nr)];
%
for ii=1:5
    lstar_over_R=0.01*2^(ii-1);
    lstar_=R*lstar_over_R;
    filename=['P_s_',num2str(lstar_over_R),'_',num2str(180),'.mat'];
    if exist(filename)==2
        load(filename);
        binwidth=A_file(3,anglebin);
        angle_=(1:180)-0.5.*ones(1,180);
        s_av=lstar_.*A_file(2,1:180);
        D_=R.*((1+cos(angle_./180.*pi)).^2+(sin(angle_./180.*pi)).^2).^0.5;
    else
        Ps=0; s=0;
        ['File ',filename,' does not exist.']
    end
    
    figure(4)
    
    format_string=[colors(ii),symbols(ii)];
    loglog(D_./R,s_av./lstar_,format_string)
    hold on
    
    format_string=[colors(ii),'-'];
    loglog(D_./R,(D_./lstar_).^2,format_string)
    axis([6e-3 3 3 1e5])
end

