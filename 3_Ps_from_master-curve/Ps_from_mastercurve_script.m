R=0.0045;
lstar=500e-6;
angle=50;


[s1,Ps1] = P_s_from_master_curve(R, lstar, angle-1);
[s2,Ps2] = P_s_from_master_curve(R, lstar, angle);
[s3,Ps3] = P_s_from_master_curve(R, lstar, angle+1);

figure(1)
s=(s1+s2+s3)./3;
Ps=(Ps1+Ps2+Ps3)./3;
loglog(s,Ps,'m')
hold all

%%

t=logspace(-8,4,100);


for ii=1:100
    g1(ii)=g1_from_Ps(A.lambda_m,s,Ps,lstar,t(ii));
end
%%
figure(2)

semilogx(t,g1)
hold all
%%
figure(4)
plot(t,log(g1),'ro')
hold all
axis([0 3e-4 -4 0])