function [s,Ps] = P_s_from_master_curve(R, lstar, angle)
lstar_over_R_file=0.08; % lstar/R value of the reference path length distribution file
N_anglebins=180;
filename=['P_s_',num2str(lstar_over_R_file),'_',num2str(N_anglebins),'.mat'];
lstar_file=R*lstar_over_R_file;
anglebin=round(angle);
if exist(filename)==2
    load(filename);
    binwidth=A_file(3,anglebin)
    test=A_file(2,anglebin)
    I_tot=A_file(1,anglebin)
    Ps=A_file(4:303,anglebin)./I_tot./binwidth./lstar_file;
    s=((1:300).*binwidth*lstar_file)';
    s=s./(lstar/lstar_file);
    Ps=Ps.*(lstar/lstar_file);
    Ps=Ps./(sum(Ps)*(s(2)-s(1)));
else
    P_s=0; s=0;
    ['File ',filename,' does not exist.']
end



end