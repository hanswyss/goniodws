# GonioDWS README file #


### Summary, purpose ###

This repository contains MATLAB files for analyzing light scattering data.
The code enables analysis of experiments performed in a standard goniometer setup in terms of Diffusing-Wave Spectroscopy (DWS).

* For a description of the approach, see the underlying research paper:  
  Z. Fahimi, F.J. Aangenendt, P. Voudouris, J. Mattsson, H.M. Wyss [*"Diffusing-Wave Spectroscopy in a Standard Dynamic Light Scattering Setup"*](http://www.mate.tue.nl/~wyss/home/resources/publications/2017/Fahimi_PhysRevE_2017.pdf) **Phys. Rev. E**, 96, 062611 (2017)

* If you make use of the code in this repository in your research, **please cite the above paper** as well as this bitbucket repository in your research papers on this topic.  

### Contact

* Please direct any questions or suggestions to H.M.Wyss [at] tue.nl  

  
### Overview of code  ###

The code is organized in the following folders:

- **"1_calculate_path_length_distribution"**:  
  This folder contains the code for the random walk simulations that produce the path length distributions of photons in a cylindrical cell. The output data is contained in the files *"P_s_0.01_180.mat"* to *"P_s_0.32_180.mat"* (The function *"P_s.m"* uses these files as an input for calculating the path length distribution as a function of cylinder radius, transport mean free path length, and detection angle. The file *"P_s.m"* is contained in the following folder.)  
For efficiency, the random walk code is written in C instead of Matlab. 
To analyze your own data you **do not necessarily need to run the random walk simulation yourself**; instead you can just use the output data in the files *"P_s_0.01_180.mat"* etc.  


- **"2_read_path_length_distribution"**:  
This folder contains code to read the individual path length distributions from the output files (*"P_s_0.01_180.mat"* to *"P_s_0.32_180.mat"*) produced by the random walk simulations (folder 1).  

  
- **"3_Ps_from_master-curve"**:  
  This folder contains code to read the path length distribution \\(P(s)\\) from the output files (*"P_s_0.01_180.mat"* to *"P_s_0.32_180.mat"*). Here we make use of the scaling properties of the path length distribution as a function of $l^* / R$ to extract the path length distribution based on the calculations for all values of $l*/R$ that were calculated. (i.e. we are using the master curve of all P(s) data shifted on top of each other.)
  
- **"4_get_eff_lstar_from_experiment"**:  
  This folder contains Matlab files for calculating the transport mean free path from experiments on calibration samples with known particle dynamics (i.e. known particle size and background viscosity.).

- **"DLSDWSdata_class"**:  
  This folder contains Matlab files for the 'DLSDWSdata' class, a Matlab class for working with dynamic light scattering data (both for standard DLS and for DWS). The class offers functionality for importing correlation functions into a DLSDWSdata object, for plotting the data, calculating mean-square displacements, viscoelastic response, etc.  
  Import methods are implemented for .ASC files produced by instruments from ALV GmbH (ALV-7004 or ALV-5000). For other file formats you will need to implement your own import method, or assign properties like the correlation function, the detection angle, etc. directly.   
  The functionality provided as separate functions in folders 1-4 above is also implemented here as methods of the DLSDWSdata class, for instance, the following methods are built in:  
    * "A=lstar_from_Intensity(A_)"  (reference intensities are hard-coded, this should be adjusted for each experimental setup).   
    * "[s,Ps] = P_s_from_master_curve(R,lstar,angle)"   
    * "MSD = MSD_from_g1_DWS_2(A_,g1_,lstar,R,theta)"   

  
- **"Figures_in_paper"**:  
  This folder contains the Matlab files and data for generating the figures contained in our paper [*"Diffusing-Wave Spectroscopy in a Standard Dynamic Light Scattering Setup"*](http://www.mate.tue.nl/~wyss/home/resources/publications/2017/Fahimi_PhysRevE_2017.pdf). These can be viewed as examples/test files for use of the code.  

