classdef DLSDWSdata
    % H.M. Wyss - June 2018
    %
    % Class for representing and operating on data taken with a standard
    % goniometer DLS setup, for DWS measurements and microrheology analysis.
    
    % Import methods are implemented for .ASC files produced by instruments
    % from ALV GmbH (ALV-7004 or ALV-5000).
    
    properties
        filename % filename of the file from which the data was imported.
        pathname % path of the file from which the data was imported.
        
        t_ms % Lag time in milliseconds
        t_s  % Lag time in seconds
        t_s_valid % Correlation time for "valid" data points
        corr % correlation (g2-1) for all channels
        
        g2_raw % raw, measured intensity autocorrelation function.
        g2     % g2
        g2min1_raw
        g2min1 % g2-1
        
        g1_raw     % raw, measured field autocorrelation function.
        g1         % g1 after correction, either scaled to 1 or Pusey averaged.
        g1_scaled % g1 scaled to an intercept of 1.
        Q      % Q value (in m^(-1))
        Q2     % Q^2 value in m^(-2)
        
        meanCR0 % 4 average countrates for the 4 detectors
        meanCR1
        meanCR2
        meanCR3
        avCR    % average of the (nonzero) countrates for the 4 detectors
        
        t_CR  % time column for countrate trace
        CR    % 4 countrate columns for countrate trace
        
        
        
        %other properties:
        
        MeasDate
        MeasTime
        Temp_K
        Temp_C
        visc_cp
        n_refr
        lambda_nm
        lambda_m
        angle_deg
        angle_rad
        duration
        Mode
        MonitorDiode
        
        % related to Pusey averaging:
        Pu_filenames={'Pu_filename1','Pu_filename2','Pu_filename3'};
        I_E_Pu1to3 % Array with intensities from 3 different Pusey measurements (countrate/monitor diode)
        I_E_Pu_av % Average of those 3 countrates (countrate/monitor diode)
        I_T       % Average intensity from current measurement (countrate/monitor diode)
        g1_Pu % Pusey-averaged g1
        g1_Pu_upper % Pusey averaged g1, using the highest of the three ensemble-average input intensities.
        g1_Pu_lower % Pusey averaged g1, using the lowest of the three ensemble-average input intensities.
        g2_ergodic_intercept
        g2min1_ergodic_intercept
        g1_ergodic_intercept
        g2_intercept
        g2min1_intercept
        g1_intercept
        
        
        
        % related to mean-square displacement and microrheology
        msd_m2  % MSD in m^2
        msd_mu2 % MSD in micrometer^2
        a_tracer %
        
        % DWS-related properties:
        l_star %  Transport mean free path lstar
        R=4.5e-3;      % radius of cylindrical sample cell
        %Standard setting for our particular setup (standard sample vials, outer diameter 1 cm)
        
        omega  % frequency in radians per second
        omega_valid % frequency in radians per second for valid data points
        alpha  % logarithmic slope of the MSD vs. t at time t (or omega=1/t)
        beta   % second-order logarithmic derivative of the MSD vs time
        Gs
        G_abs % Magnitude of the complex modulus in Pa
        Gp % Storage modulus in Pa
        Gpp    % Loss modulus in Pa
        G_raw     % Raw modulus value obtained from kT/pi/a_tracer./msd
        t_murheo % Corresponding time vector
        
        t_smooth  % smoothed time vector (same as t_murheo)
        msd_smooth % smoothed msd vector
        
        % related to file export, plots etc.
        output_string
        label
        
    end
    
    methods
        
        
        
        function IC = intercept_g1(A_)
            IC=A_.intercept(A_.t_s,A_.g1);
        end
        
        function IC = intercept_g2(A_)
            IC=A_.intercept(A_.t_s,A_.g2);
        end
        
        function IC = intercept_g2min1(A_)
            IC=A_.intercept(A_.t_s,A_.g2min1);
        end
        
        
        
        function A = PuseyAverage(A_, PuFile1, PuFile2, PuFile3)
            A=A_;
            A.Pu_filenames={PuFile1, PuFile2, PuFile3};
            APu1=DLSDWSdata.ImportFromFile(PuFile1);APu2=DLSDWSdata.ImportFromFile(PuFile2);APu3=DLSDWSdata.ImportFromFile(PuFile3);
            
          
            I_E1=APu1.avCR/APu1.MonitorDiode;I_E2=APu2.avCR/APu2.MonitorDiode;I_E3=APu3.avCR/APu3.MonitorDiode;
            % Ensemble-averaged intensity:
            I_E_=(I_E1+I_E2+I_E3)/3;
            % Time-averaged intensity:
            I_T_=A_.avCR/A_.MonitorDiode;
            Y=I_E_/I_T_;
            
            Y_upper=max([I_E1,I_E2,I_E3])/I_T_;
            Y_lower=min([I_E1,I_E2,I_E3])/I_T_;
            
            
            intercept_=A.intercept(A.t_s,A.g2min1_raw)./A.g2min1_ergodic_intercept;
            
            
            A.g2min1=A.g2min1./A.intercept(A.t_s,A.g2min1);
            A.g1_Pu=real((Y-1)/Y+((A.g2min1+1-intercept_).^0.5)/Y);
            A.g1_Pu(A.g1_Pu==0)=NaN;
            A.g1=A.g1_Pu;
            
            A.g1_Pu_upper=real((Y_upper-1)/Y_upper+((A_.g2-intercept_).^0.5)/Y_upper);
            A.g1_Pu_upper(A.g1_Pu==0)=NaN;
            
            A.g1_Pu_lower=real((Y_lower-1)/Y_lower+((A_.g2-intercept_).^0.5)/Y_lower);
            A.g1_Pu_lower(A.g1_Pu==0)=NaN;
            A.I_E_Pu_av=I_E_;
            A.I_E_Pu1to3=[I_E1;I_E2;I_E3];
            A.I_T=I_T_;
        end
        
        function A = calc_G(A_,varargin)
            % This function calculates viscoelastic moduli from the motion
            % of tracer particles (of radius a) in the material.
            % The calculation is based on the equations given in the paper
            % Dasgupta et al. Journal of
            
            %Input option 1:
            % no additional input parameters; A = A.calc_G() or A = A.calc_G
            %Input option 2:
            % particle radius in m as additional input parameter; A = A.calc_G(0.3e-6)
            
            % a: particle radius in meters
            
            A=A_;
            
            switch nargin
                
                case 1 % no input arguments except the source object.
                    
                    if isempty(A.a_tracer)
                        message='Must provide a value for the tracer particle radius a (in m).';
                        h = msgbox( message );
                        return
                    end
                    
                case 2
                    if isempty(A.a_tracer)
                        A.a_tracer=varargin{1};
                    else
                        %message=['Overwriting the old value of the tracer particle radius ... ( a=',num2str(A.a_tracer),' ).'];
                        %h = msgbox( message );
                        A.a_tracer=varargin{1};
                    end
                    
                otherwise
                    message=['Too many input arguments!'];
                    h = msgbox( message );
                    return
            end
            
            A.omega=1./A.t_s;
            kB=1.38065e-23;
            Temp=A.Temp_K;
            a=A.a_tracer;
            t=A.t_s;
            g1=A.g1;
            
            
            msd=A.msd_m2;
            dim=3; %dimensionality of the data.
            width=0.3; %width of the smoothing Gaussian weight distribution
            [msd_smooth_,df,ddf] = DLSDWSdata.logderive(t,msd,width);
            df=df'; ddf=ddf'; msd_smooth_=msd_smooth_';
            
            %             width=4;
            %             [msd_smooth_,df,ddf]=DLSDWSdata.logpolyfit_msd(t,msd,width);
            
            
            
            df=real(df);ddf=real(ddf);msd_smooth_=real(msd_smooth_);
            A.alpha=df;
            A.beta=ddf;
            oneone=ones(size(df));
            %size(oneone), size(df), size(ddf)
            arr1=gamma(oneone+df).^(-1);
            %             arr2=1./(oneone+ddf);
            % %           arr3=1./msd_smooth_;
            arr3=msd.^(-1);
            %size(arr1), size(arr2),size(arr3)
            A.G_abs=arr1.*arr3.*dim*kB*Temp/3/pi/A.a_tracer;
            A.G_abs=A.G_abs.*arr1;
            A.msd_smooth=msd_smooth_;
            
            % Storage and Loss moduli:
            
            A.Gp=A.G_abs.*cos(pi/2*df);
            A.Gpp=A.G_abs.*sin(pi/2*df);
            
            range1=A.Gp<0.01.*A.Gpp;
            A.Gp(range1)=NaN;
            range2=A.Gpp<0.01.*A.Gp;
            A.Gpp(range2)=NaN;
            
            
        end
        
        function report(A_)
            col1=A_.t_s; col1n=['t [s] (',A_.label,')'];
            col2=A_.g1; col2n=['g1 (',A_.label,')'];
            col3=A_.g1_raw; col3n=['raw g1 (',A_.label,')'];
            col4=A_.g1_Pu; col4n=['g1 Pusey averaged [rad/s] (',A_.label,')'];
            
            data=[col1, col2, col3, col4];
            [pathstr,name,ext] = fileparts(A_.filename);
            name=[name,'_',num2str(round(A_.Temp_C*100)/100),'C_',num2str(round(A_.angle_deg*100)/100),'deg_'];
            outfile=fullfile(A_.pathname,[name,'.txt']);
            %save(outfile_corr,'data','-ascii', '-double', '-tabs')
            header=sprintf([ col1n, '\t',col2n, '\t',col3n, '\t',col4n]);
            dlmwrite(outfile,header,'delimiter','\t');
            dlmwrite(outfile,data,'delimiter','\t','-append');
            
        end
        
        function h = g1_plot(A_,format_string)
            %g1_plot
            x_label='$t$ [s]';
            y_label='$f(q,t)$';
            h=semilogx(A_.t_s,A_.g1,format_string);
            hold on
        end
        
        function [a_,tau_] = InitialSlopeAnalysis(A_,range)
            x=A_.t_s(range); y=A_.g1(range);
            expfit=fit(x,y,'exp1');
            figure(1)
            clf
            figure(1)
            semilogy(x,y,'o')
            hold all
            a_=expfit.a;
            tau_=-1/expfit.b
            semilogy(x,a_*exp(-x./tau_),'-')
            
        end
        
        function A = lstar_from_Intensity(A_)
            % values taken from calibration measurements on our ALV system, and comparisons
            % with simulations.
            % Radius of the sample cell:
            R=A_.R;
            % Maximum Monitor diode reading (corresponding to 100% transmission of the
            % attenuator):
            MonDiode_Max=3.1761e+06;
            
            I_ref=...
                MonDiode_Max*1e-3.*[[ 0.0026  ,  0.0027  ,  0.0030  ,  0.0034  ,  0.0040  ,  0.0048  ,  0.0060  ,  0.0077  ,  0.0105  ,  0.0153  ,  0.0236 ,    0.0409  ,  0.0857];
                [ 0.0040  ,  0.0043  ,  0.0047  ,  0.0053  ,  0.0061  ,  0.0074  ,  0.0092  ,  0.0115  ,  0.0158  ,  0.0229  ,  0.0351  ,    0.0605  ,  0.1245];
                [ 0.0053  ,  0.0056  ,  0.0062  ,  0.0069  ,  0.0080  ,  0.0096  ,  0.0119  ,  0.0148  ,  0.0205  ,  0.0295  ,  0.0453  ,    0.0777  ,  0.1592];
                [ 0.0069  ,  0.0076  ,  0.0080  ,  0.0094  ,  0.0110  ,  0.0131  ,  0.0160  ,  0.0201  ,  0.0275  ,  0.0392  ,  0.0576  ,    0.1017  ,  0.2017];
                [ 0.0104  ,  0.0112  ,  0.0122  ,  0.0137  ,  0.0158  ,  0.0187  ,  0.0230  ,  0.0289  ,  0.0392  ,  0.0552  ,  0.0824  ,    0.1356  ,  0.2529];
                [ 0.0143  ,  0.0154  ,  0.0167  ,  0.0187  ,  0.0216  ,  0.0253  ,  0.0311  ,  0.0384  ,  0.0518  ,  0.0713  ,  0.1034  ,    0.1627  ,  0.2648];
                [ 0.0213  ,  0.0225  ,  0.0246  ,  0.0274  ,  0.0309  ,  0.0363  ,  0.0436  ,  0.0527  ,  0.0694  ,  0.0915  ,  0.1248  ,    0.1810  ,  0.2746]]   ;
            
            ang_deg_ref=(30:10:150)';
            l_star_divR_ref=[0.1574  ;  0.2259 ;   0.2844  ;  0.3811  ;  0.5299  ;  0.7161  ;  0.9788].*1.0e-03/R;
            I_ref_myangle=interp1(ang_deg_ref, I_ref',A_.angle_deg)';
            if A_.I_E_Pu_av>0
                I_CR=A_.I_E_Pu_av*MonDiode_Max;
                %I_CR=A_.I_T*MonDiode_Max;
            else
                I_CR=A_.I_T*MonDiode_Max;
            end
            % Get lstar from intensity vs. lstar curve
            lstar=interp1(I_ref_myangle,l_star_divR_ref,I_CR)*A_.R; % by interpolation from the list.
            if isnan(lstar)
                % by linear fit function:
                fit1=polyfit(I_ref_myangle,l_star_divR_ref,1);
                lstar=(fit1(1)*I_CR+fit1(2))*A_.R;
            end
            A=A_;
            A.l_star=lstar;
            A.output_string  % Print output string to screen as a visual feedback for the user.
        end
        
    end
    
    
    methods(Static=true)
        
        function g1_Pu_ = PuseyAverage_(g2, delta2, Y)
            
            g1_Pu_=real((Y-1)/Y+((g2-delta2).^0.5)/Y);
            
        end
        
        
        function A=uiImport()
            [filen, pathn] = uigetfile( {'*.ASC'},'Pick a .ASC file to import.');
            filename=fullfile(pathn, filen);
            A=DLSDWSdata.ImportFromFile(filename);
            figure(1);
            clf;
            figure(1);
            semilogx(A.t_s,A.g1);
        end
        
        function A=ImportFromFile(filename)
            
            %
            A=DLSDWSdata;
            A.filename=filename;
            [A.pathname,filen,extn]=fileparts(filename);
            
            
            fid=fopen(filename);
            textline=fgets(fid);
            
            if strncmp(textline,'ALV-7004',8)==true   % recognize file for "new" ALV instrument CGS-4
                file_type=2;
            elseif strncmp(textline,'ALV-5000',8)==true % recognize file for "old" ALV instrument
                file_type=3;
            else
                h = msgbox('wrong format of source file.');
                fclose(fid);
                return
            end
            
            while strncmp(textline,'"Correlation"',11)==false
                textline=fgets(fid);
                %--------------------------------------------
                % read MeasDate:
                if strncmp(textline,'Date :',6)==true
                    A.MeasDate=sscanf(textline,'Date : %s');
                end
                %--------------------------------------------
                % read MeasTime:
                if strncmp(textline,'Time :',6)==true
                    A.MeasTime=sscanf(textline,'Time : %s');
                end
                %--------------------------------------------
                % read Temp_K
                if strncmp(textline,'Temperature [K] :',17)==true
                    A.Temp_K=sscanf(textline,'Temperature [K] : %f');
                end
                A.Temp_C=A.Temp_K-273.15;
                %--------------------------------------------
                % read visc_cp
                if strncmp(textline,'Viscosity [cp]  :',17)==true
                    A.visc_cp=sscanf(textline,'Viscosity [cp]  : %f');
                end
                %--------------------------------------------
                % read n_refr
                if strncmp(textline,'Refractive Index:',17)==true
                    A.n_refr=sscanf(textline,'Refractive Index: %f');
                end
                %--------------------------------------------
                % read lambda_nm
                if strncmp(textline,'Wavelength [nm] :',17)==true
                    A.lambda_nm=sscanf(textline,'Wavelength [nm] : %f');
                end
                A.lambda_m=A.lambda_nm*1E-9;
                
                %--------------------------------------------
                % read angle_deg
                if strncmp(textline,'Angle',5)==true
                    A.angle_deg=sscanf(textline(18:length(textline)),' %f');
                end
                A.angle_rad=A.angle_deg/180*pi;
                
                %--------------------------------------------
                % read duration
                if strncmp(textline,'Duration [s]    :',17)==true
                    A.duration=sscanf(textline,'Duration [s]    : %f');
                end
                %--------------------------------------------
                % read Mode
                if strncmp(textline,'Mode            :',17)==true
                    A.Mode=sscanf(textline,'Mode            : %s');
                end
                %--------------------------------------------
                % read meanCR0
                if strncmp(textline,'MeanCR0 [kHz]   :',17)==true
                    A.meanCR0=sscanf(textline,'MeanCR0 [kHz]   : %f');
                end
                %--------------------------------------------
                % read meanCR1
                if strncmp(textline,'MeanCR1 [kHz]   :',17)==true
                    A.meanCR1=sscanf(textline,'MeanCR1 [kHz]   : %f');
                end
                CRs_=[A.meanCR0,A.meanCR1,A.meanCR2,A.meanCR3];
                A.avCR=mean(CRs_(CRs_>0));
                
            end
            
            switch file_type
                
                case 2
                    t_=zeros(1000,1);ii=0;
                    corr_=zeros(1000,4);
                    while strncmp(textline,'"Count Rate"',12)==false && ii<1001
                        textline=fgets(fid); ii=ii+1;
                        x=sscanf(textline,'   %f');
                        xlength=length(x);
                        if(xlength>1)
                            t_(ii,1)=x(1);
                            corr_(ii,1:(xlength-1)) = x(2:xlength);
                        end
                    end
                    sel=t_(:,1)>0;
                    A.t_ms=t_(sel,1);A.corr=corr_(sel,1:4);
                    A.t_s=A.t_ms./1000;
                    % read count rate trace:
                    t_CR_=zeros(1000,1);ii=0;
                    CR_=zeros(1000,4);
                    while strncmp(textline,'Monitor Diode',13)==false
                        textline=fgets(fid);
                        ii=ii+1;
                        x=sscanf(textline,'       %f');
                        xlength=length(x);
                        if(xlength>1)
                            t_CR_(ii,1)=x(1);
                            CR_(ii,1:(xlength-1)) = x(2:xlength);
                        end
                    end
                    sel=t_CR_(:,1)>0;
                    selCR=CR_(1,:)>0;
                    A.t_CR=t_CR_(sel,1);A.CR=CR_(sel,selCR);
                    % read monitor diode:
                    A.MonitorDiode=sscanf(textline,'Monitor Diode	%f');
                    fclose(fid);  % Closing file
                    corr_=A.corr(:,mean(A.corr)>0);
                    A.g2min1_raw=mean(corr_')';
                    
                case 3
                    % read correlation functions:
                    t_=zeros(1000,1);ii=0;
                    corr_=zeros(1000,1);
                    while strncmp(textline,'"Count Rate"',12)==false && ii<1001
                        textline=fgets(fid); ii=ii+1;
                        x=sscanf(textline,'   %f');
                        if(length(x)>1)
                            t_(ii,1)=x(1);
                            corr_(ii,1) = x(2);
                        end
                    end
                    sel=t_(:,1)>0;
                    A.t_ms=t_(sel,1);A.corr=corr_(sel,1);
                    A.t_s=A.t_ms./1000;
                    % read count rate trace:
                    t_CR_=zeros(1000,1);ii=0;
                    CR_=zeros(1000,4);
                    while strncmp(textline,'Monitor Diode',13)==false
                        textline=fgets(fid); ii=ii+1;
                        x=sscanf(textline,'   %f');
                        
                        if(length(x)>1)
                            t_CR_(ii,1)=x(1);
                            CR_(ii,2:(length(x))) = x(2:(length(x)));
                        end
                    end
                    sel=CR_(:,1)>0;
                    A.t_CR=t_CR_(sel,1);A.CR=CR_(sel,1:4);
                    % read monitor diode:
                    A.MonitorDiode=sscanf(textline,'Monitor Diode	%f');
                    fclose(fid); % closing file
                    
                    A.g2min1_raw=A.corr;
            end
            
            % Raw correlation functions
            A.g2min1_raw(A.g2min1_raw<0)=NaN; % get rid of negative entries
            A.g2_raw=A.g2min1_raw+1;
            A.g1_raw=sqrt(A.g2min1_raw);
            
            
            % Intercepts of raw correlation functions:
            A.g2min1_intercept=A.intercept(A.t_s,A.g2min1_raw);
            A.g2_intercept=A.g2min1_intercept+1;
            A.g1_intercept=sqrt(A.g2min1_intercept);
            A.g1_scaled=A.g1_raw./A.g1_intercept;
            
            % Intensities:
            A.I_T=A.avCR/A.MonitorDiode;
            
            % calculate Q:
            A.Q=4*pi*A.n_refr/A.lambda_m*sin(A.angle_rad/2);
            A.Q2=A.Q^2;
            
            % set standard values for output_string and label:
            [pathn,filen,ext] = fileparts(A.filename);
            A.output_string=filen; % cut of file extension
            A.label=['T = ',num2str(round(A.Temp_C*100)/100),' [C], angle = ',num2str(round(A.angle_deg*100)/100),' $^\circ$'];
            
            
            % Check if Pusey folder exists, if yes: perform Pusey averaging
            puseyfolder=[filen,'_Pu'];
            puseyfolder=fullfile(pathn,puseyfolder);
            if exist(puseyfolder,'dir')==7
                puseyfiles=dir([puseyfolder,'/*.ASC']);
                A.Pu_filenames{1}=fullfile(puseyfolder,puseyfiles(1).name);
                A.Pu_filenames{2}=fullfile(puseyfolder,puseyfiles(2).name);
                A.Pu_filenames{3}=fullfile(puseyfolder,puseyfiles(3).name);
                if size(puseyfiles,1)==3
                    ergodic_=false;
                else
                    ergodic_=false;
                    waitfor(msgbox('The script expects exactly 3 .ASC files in the Pusey folder...'))
                end
            else
                ergodic_=true;
                %waitfor(msgbox('Pusey folder does not exist'));
                A.g1=A.g1_scaled;
            end
            
            
            
            switch ergodic_
                
                case true
                    % intercepts
                    A.g2_ergodic_intercept=A.g2_intercept;
                    A.g2min1_ergodic_intercept=A.g2min1_intercept;
                    A.g1_ergodic_intercept=A.g1_intercept;
                    
                    A.g2min1=A.g2min1_raw./A.g2min1_ergodic_intercept;
                    A.g2=A.g2min1+1;
                    A.g1=(A.g1_raw)./A.g1_ergodic_intercept;
                    
                    
                case false
                    % Import Pusey files
                    for ii=1:3
                        A.Pu_filenames{ii}
                        APu(ii)=A.ImportFromFile(A.Pu_filenames{ii});
                    end
                    %A.g2min1_ergodic_intercept=mean([APu(1).g2min1_ergodic_intercept, APu(2).g2min1_ergodic_intercept,APu(3).g2min1_ergodic_intercept]);
                    %A.g2min1_ergodic_intercept=0.9;
                    ergodic_intercept_file=fullfile(A.pathname,'ergodic_g2min1_intercept.txt');
                    if exist(ergodic_intercept_file)==2
                        A.g2min1_ergodic_intercept=load(ergodic_intercept_file,'-ascii');
                    else
                        A.g2min1_ergodic_intercept=mean([APu(1).g2min1_ergodic_intercept, APu(2).g2min1_ergodic_intercept,APu(3).g2min1_ergodic_intercept]);
                    end
                    
                    A.g2_ergodic_intercept=A.g2min1_ergodic_intercept+1;
                    A.g1_ergodic_intercept=sqrt(A.g2min1_ergodic_intercept);
                    
                    % Intensities
                    I_E1=APu(1).avCR/APu(1).MonitorDiode;I_E2=APu(2).avCR/APu(2).MonitorDiode;I_E3=APu(3).avCR/APu(3).MonitorDiode;
                    A.I_E_Pu1to3=[I_E1,I_E2,I_E3];
                    % Ensemble-averaged intensity:
                    A.I_E_Pu_av=mean(A.I_E_Pu1to3);
                    %
                    Y=A.I_E_Pu_av/A.I_T;
                    delta2=A.g2min1_intercept/A.g2min1_ergodic_intercept;
                    A.g2min1=A.g2min1_raw./A.g2min1_ergodic_intercept;
                    A.g2=A.g2min1+1;
                    A.g1=A.g1_raw./A.g1_ergodic_intercept;
                    
                    A.g1=DLSDWSdata.PuseyAverage_(A.g2, delta2, Y);
                    A.g2min1=A.g1.^2;
                    A.g2=A.g2+1;
                    
            end
            
            
            % DWS analysis:
            
            % extract lstar from intensity:
            
            % To set a fixed lstar value, put a text file named l_star.txt
            % into the directory..
            
            lstarfile=fullfile(A.pathname,'l_star.txt');
            if exist(lstarfile)==2
                ls=load(lstarfile,'-ascii');
                A.l_star=ls;
                %A.l_star=5.4994e-04; % Override lstar XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            else
                A=A.lstar_from_Intensity;
            end
            
            
            % Get path length distribution from calibration files:
            %[s,Ps]=P_s_from_master_curve(A.R,A.l_star,A.angle_deg);
            
            % Calculate mean-square displacement of tracers from g1(t), path
            % length distribution, and lstar. :
            %A.msd_m2=DLSDWSdata.MSD_from_g1_DWS_2(A);
            %A.msd_mu2=A.msd_m2*1e12;
            
            t=A.t_s;
            g1=A.g1;
            theta=A.angle_deg;
            Npts=length(g1);
            
            
            % Tracer particle size:
            tracerfile=fullfile(A.pathname,'tracer_radius.txt');
            if exist(tracerfile)==2
                a=load(tracerfile,'-ascii');
                A.a_tracer=a;
            end
            
            if A.a_tracer>0
                MSD=ones(size(A.g1));
                for ii=1:Npts
                    if isnan(A.g1(ii))
                        MSD(ii)=NaN;
                    else
                        MSD(ii)=MSD_from_g1_DWS_2(A.g1(ii),A.l_star,A.R,theta);
                    end
                    %[ii,Npts]
                end
                
                A.msd_m2=MSD;
                A.msd_mu2=A.msd_m2*1e12;
                
                
                %A.msd_m2
                if size(A.msd_m2)>0
                A=A.calc_G(A.a_tracer);
                end
                
                
                A.t_s_valid=A.t_s;
                A.omega_valid=A.omega;
                IC=A.intercept(A.t_s,A.g1_raw);
                A.t_s_valid(A.g1_raw > 0.99*IC)=NaN; %exclude points with g1_raw too close to the intercept
                A.t_s_valid(A.g1_raw < 0.05*IC)=NaN;
                A.t_s_valid(isnan(A.g1_raw))=NaN;
                %exclude points with g1_raw too close to zero.
                
                A.t_s_valid(A.t_s_valid==0)=NaN;
                A.omega_valid=A.t_s_valid.^(-1);
            end
            
        end  % end of function ImportFromFile
        
       
        function IC = intercept(t_,f_)
            range=1:30;
            fit=polyfit(t_(range),log(f_(range)),1);
            IC=exp(fit(2));
        end
        
        function A_array=EvalAllInFolder(theFolder)
            close all
            if exist(theFolder,'dir')==7
                ASCfiles=dir([theFolder,'/*.ASC']);
                nFiles=size(ASCfiles,1);
                if nFiles>0
                    A_array=DLSDWSdata(nFiles);
                    for ii=1:nFiles
                        theFile=fullfile(theFolder,ASCfiles(ii).name);
                        A_array(ii)=DLSDWSdata.ImportFromFile(theFile);
                    end
                else
                    waitfor(msgbox('No files..'));
                    A_array=DLSDWSdata;
                end
            end
            
            % Replace the lstar values with the average over the whole
            % series.
            
            %             ls_=zeros(nFiles,1);
            %             for ii=1:nFiles
            %                 ls_(ii)=A_array(ii).l_star;
            %             end
            %             lstar=mean(ls_);
            %             for ii=1:nFiles
            %                 A_array(ii).l_star=lstar;
            %             end
            
            
            
            % Extract and output data about this set of measurements:
            
            
            % Get the name of the parent folder (name of the measurement
            % series)
            [parent_path, parentfoldername, ~] = fileparts(theFolder); %fileparts(cd);
            %evalfolder=fullfile(parent_path,parentfoldername,[A_array(ii).filename,'_eval']);
            
            % Save array of objects as mat file:
            outfile=[parentfoldername,'_LS_data_series.mat'];
            fulloutfile=fullfile(theFolder,outfile);
            save(fulloutfile,'A_array');
            
            % evalfolder to save output for individual files:
            for ii=1:nFiles
                [fpath,fname]=fileparts(A_array(ii).filename);
                evalfolder{ii}=fullfile(fpath,[fname,'_eval']);
                if not(exist(evalfolder{ii},'dir')==7) % if eval folder does not exist, create it.
                    mkdir(evalfolder{ii});
                end
                
            end
            
            % save data for individual file to .mat file
            
            for ii=1:nFiles
                A_=A_array(ii);
                outputfile=fullfile(evalfolder{ii},[A_array(ii).output_string,'.mat']);
                save(outputfile,'A_');
            end
            
            %save g1 series plot
            h1=figure(1);
            clear legend_
            for ii=1:nFiles
                A_=A_array(ii);
                semilogx(A_.t_s_valid,A_.g1,'o');
                hold all
                legend_{2*ii-1}=A_.label;
                legend_{2*ii}=' ';
                semilogx(A_.t_s,A_.g1_raw,':');
            end
            g1_graph_title_=parentfoldername;
            g1_xlabel_='$t$ [s]';
            g1_ylabel_='$f(q,t)$';
            
            legend(legend_);
            DLSDWSdata.nicify_plot(h1,g1_graph_title_,g1_xlabel_,g1_ylabel_);
            set(gcf,'PaperOrientation','landscape');
            set(gcf,'PaperPositionMode','auto');
            outfile=[parentfoldername,'_g1_vs_t_all','.pdf'];
            axis([1e-8 10 0 1.05]);
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            
            %save msd_m2 series plot
            h2=figure(2)
            
            clear legend_
            for ii=1:nFiles
                A_=A_array(ii);
                loglog(A_.t_s_valid,A_.msd_m2,'o');
                hold all;
                legend_{2*ii-1}=A_.label;
                legend_{2*ii}=' ';
                loglog(A_.t_s,A_.msd_m2,':');
            end
            msd_m2_graph_title_=parentfoldername;
            msd_m2_xlabel_='$t$ [s]';
            msd_m2_ylabel_='MSD$(t)$ [m$^2$]';
            
            legend(legend_);
            DLSDWSdata.nicify_plot(h2,msd_m2_graph_title_,msd_m2_xlabel_,msd_m2_ylabel_);
            
            set(gcf,'PaperOrientation','landscape');
            set(gcf,'PaperPositionMode','auto');
            outfile=[parentfoldername,'_msd_m2_vs_t_all','.pdf'];
            axis([1e-8 10 1e-18 1e-13]);
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            outfile=[parentfoldername,'_msd_m2_vs_t_all','.fig'];
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            
            %save G_abs series plot
            h3=figure(3);
            
            clear legend_
            for ii=1:nFiles
                A_=A_array(ii);
                if size(A_.omega_valid)==size(A_.G_abs)
                    loglog(A_.omega_valid,A_.G_abs,'o');
                    hold all;
                    legend_{2*ii-1}=A_.label;
                    legend_{2*ii}=' ';
                    loglog(A_.omega,A_.G_abs,':');
                end
            end
            G_abs_graph_title_=parentfoldername;
            G_abs_xlabel_='Frequency $\omega$ [s$^-1$]';
            G_abs_ylabel_='Modulus $|G^*(\omega)|$';
            axis([1e-1 1e6 1e-2 1e5]);
            if size(A_.omega_valid)==size(A_.G_abs)
                legend(legend_);
            end
            DLSDWSdata.nicify_plot(h3,G_abs_graph_title_,G_abs_xlabel_,G_abs_ylabel_);
            set(gcf,'PaperOrientation','landscape');
            set(gcf,'PaperPositionMode','auto');
            outfile=[parentfoldername,'_G_abs_vs_t_all','.pdf'];
            fulloutfile=fullfile(theFolder,outfile);
            saveas(gcf,fulloutfile);
            
            %outputfilename=[graph_title,'.jpg'];
            %saveas(gcf,outputfilename);
        end
        
        function h=nicify_plot(h,graph_title_,xlabel_,ylabel_)
            
            figure(h)
            
            set(gca,'LineWidth',1);
            set(gca,'FontSize',14);
            xlabel(xlabel_,'Fontsize',24,'Interpreter','latex');
            ylabel(ylabel_,'Fontsize',24,'Interpreter','latex');
            title(graph_title_,'Interpreter','latex');
            set(gcf, 'PaperUnits', 'centimeters');
            set(gcf, 'Units', 'centimeters');
            set(gcf, 'Position', [2 2 17 17]);
            set(gca, 'Units', 'centimeters');
            legend('Location','Best');
            set(gca, 'Position', [3 3 12 12]);
            legend(gca,'show');
            legend(gca,'boxoff');
            LEGH = legend;
            set(LEGH,'FontSize',10);
            set(LEGH,'Interpreter','latex');
            set(get(gca,'Children'),'linewidth',1); % This sets the linewidth for all plots. (plots are 'Children' of the current axis.
            set(gcf,'PaperOrientation','portrait');
            set(gcf,'PaperPositionMode','auto');
        end
        
        function obj = DLSDWSdata(n) %primitive class constructor
            if nargin > 0
                if n>0
                    for ii=1:n
                        obj(ii)=DLSDWSdata;
                    end
                end
            end
        end
        
        function [s,Ps] = P_s_from_master_curve(R,lstar,angle)
            lstar_over_R_file=0.08; % lstar/R value of the reference path length distribution file
            N_anglebins=180;
            filename=['P_s_',num2str(lstar_over_R_file),'_',num2str(N_anglebins),'.mat'];
            lstar_file=R*lstar_over_R_file;
            anglebin=round(angle);
            if exist(filename)==2
                load(filename);
                binwidth=A_file(3,anglebin);
                test=A_file(2,anglebin);
                I_tot=A_file(1,anglebin);
                Ps=A_file(4:303,anglebin)./I_tot./binwidth./lstar_file;
                s=((1:300).*binwidth*lstar_file)';
                s=s./(lstar/lstar_file);
                Ps=Ps.*(lstar/lstar_file);
                Ps=Ps./(sum(Ps)*(s(2)-s(1)));
            else
                P_s=0; s=0;
                ['File ',filename,' does not exist.']
            end
            
        end
        
        function MSD = MSD_from_g1_DWS_2(A_,g1_,lstar,R,theta)
            
            if isnan(g1_)
                MSD=NaN;
            else
                [s,Ps]=DLSDWSdata.P_s_from_master_curve(R, lstar, theta);
                lambda=A_.lambda_m;
                k0=2*pi*A_.n_refr/lambda;
                
                
                MSD_low=1e-50;
                MSD_high=1e20;
                
                N_s=length(Ps);
                
                
                MSD=NaN;
                MSD_low=1e-50;
                MSD_high=1e20;
                MSD_mid=exp((log(MSD_high)+log(MSD_low))/2);
                
                N_s=length(Ps);
                
                g1_low=0;
                g1_high=0;
                g1_mid=0;
                for ii=2:N_s
                    g1_low=g1_low+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_high*s(ii)/lstar);
                    g1_high=g1_high+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_low*s(ii)/lstar);
                    g1_mid=g1_mid+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_mid*s(ii)/lstar);
                end
                
                
                MSD=NaN;
                
                while abs((g1_high-g1_low)/g1_high)>0.001
                    MSD_mid=exp((log(MSD_high)+log(MSD_low))/2);
                    g1_low=0;
                    g1_high=0;
                    g1_mid=0;
                    for ii=2:N_s
                        g1_low=g1_low+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_high*s(ii)/lstar);
                        g1_high=g1_high+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_low*s(ii)/lstar);
                        g1_mid=g1_mid+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_mid*s(ii)/lstar);
                    end
                    if g1_mid<g1_
                        MSD_high=MSD_mid;
                    end
                    if g1_mid>g1_
                        MSD_low=MSD_mid;
                    end
                    if MSD_high<MSD_low
                        MSD_t=MSD_low;
                        MSD_low=MSD_high;
                        MSD_high=MSD_t;
                    end
                end
                MSD=(MSD_high+MSD_low)/2;
                
            end
            
        end
        
        function [f2,df,ddf] = logderive(x,f,width)
            
            %       A special purpose routine for finding the first and
            %       second logarithmic derivatives of slowly varying,
            %       but noisy data.  It returns 'f2'-- a smoother version
            %       of 'f' and the first and second log derivative of f2.
            
            np = length(x);
            df = zeros(1,np);
            ddf = zeros(1,np);
            f2 = zeros(1,np);
            lx = log(x);
            ly = log(f);
            
            for i=1:np
                w = exp( -(lx-lx(i)).^2 / (2*width.^2) );         % a 'Gaussian'
                ww = find(w > 0.03);                           % truncate the gaussian, run faster
                %s(i)=length(ww);
                res = DLSDWSdata.polyfitw(lx(ww),ly(ww),w(ww),2);
                f2(i) = exp(res(3) + res(2)*lx(i) + res(1)*(lx(i)^2));
                df(i) = res(2)+(2*res(1)*lx(i));
                ddf(i) = 2*res(1);
            end
            
            %             for i=1:np
            %                 w = exp( -(lx-lx(i)).^2 / (2*width.^2) );         % a 'Gaussian'
            %                 ww = find(w > 0.03);                           % truncate the gaussian, run faster
            %                 %s(i)=length(ww);
            %                 res = DLSDWSdata.polyfitw(lx(ww),ly(ww),w(ww),1);
            %                 f2(i) = exp(res(2) + res(1)*lx(i) );
            %                 df(i) = res(1);
            %                 ddf(i) = 0;
            %             end
        end
        
    
    
    
        
        function [line,marker,color]=LSLineMarkerColor(ii)
            
            colors = {'k', 'b', 'g', 'r', 'm'};
            markers = {'o', 'x', '+', '*', 's', 'd', 'v', '^', '<', '>', 'p'};
            lines = {'-', '-.', '--'};
            line = lines{mod(ii-1, length(lines))+1};
            marker = markers{mod(ii-1, length(markers))+1};
            color = colors{mod(ii-1, length(colors))+1};
        end
        
        function linestyle_=LSlinestyle(ii)
            [line,marker,color]=DLSDWSdata.LSLineMarkerColor(ii);
            linestyle_=[line,color];
        end
        
        function linestyle_=LSsymbolstyle(ii)
            [line,marker,color]=DLSDWSdata.LSLineMarkerColor(ii);
            linestyle_=[marker,color];
        end
        
        
        
        function [msd_smooth,dlogy,ddlogy]=logpolyfit_msd(t,msd,width)
            
            % Returns the first and second derivatives of log(y) as a function of
            % log(x), dlogy and ddlogy.
            % INPUTS:
            % t: a vector of time values (sorted from low to high values).
            % msd: a vector with the corresponding mean-square displacement values.
            % The following bounds are set on the outputs, for physical reasons:
            % dlogy<=1  : as values larger than 1 would indicate super-diffusive
            % behavior
            % dlogy>=0 :
            % width: number of points around each point, to be considered in the local
            % polynomial fit.
            % OUTPUTS:
            % y_smooth: smoothed version of y, taken from each individual fit
            % (This should be an interger value, width>1; standard: width around 3-5)
            nPtsAll=length(t);
            x=log(t);y=log(msd);
            validpts=~isnan(y);
            nanpts=isnan(y);
            x=x(validpts);y=y(validpts);
            nPts=length(y);
            % Initialize arrays
            msd_smooth_valid=ones(nPts,1);
            msd_smooth=ones(nPtsAll,1);
            dlogy_valid=ones(nPts,1);ddlogy_valid=ones(nPts,1);
            dlogy=ones(nPtsAll,1);ddlogy=ones(nPtsAll,1);
            
            
            for ii=1:nPts
                range=(max(1,(ii-width))):min(nPts,ii+width);
                % Fitting in two steps: first a linear local fit, then, keeping the
                % slope constant, a second order polynomial fit (only the first and the
                % third parameter are varied.
                %f1=fit(x(range,1),y(range,1),'poly1','Upper',[1 inf],'Lower',[0 -inf]);
                %f2=fit(x(range,1),y(range,1),'p1*(x-p2)^2+p3*x+p4','Upper',[inf f1.p1 x(ii) inf],'Lower',[-inf f1.p1 x(ii) -inf]);
                f2=fit(x(range,1),y(range,1),'poly2','Upper',[inf inf inf],'Lower',[-inf -inf -inf]);
                msd_smooth_valid(ii)=exp(f2(x(ii)));
                dlogy_valid(ii)=x(ii)*2*f2.p1+f2.p2;
                ddlogy_valid(ii)=2*f2.p1;
            end
            dlogy(validpts)=dlogy_valid; %reassign NaN's to keep vectors at the same length... :
            if nPtsAll>nPts
                dlogy(nanpts)=ones(nPtsAll-nPts,1).*NaN;
            end
            ddlogy(validpts)=ddlogy_valid;
            if nPtsAll>nPts
                ddlogy(nanpts)=ones(nPtsAll-nPts,1).*NaN;
            end
            msd_smooth(validpts)=msd_smooth_valid;
            if nPtsAll>nPts
                msd_smooth(nanpts)=ones(nPtsAll-nPts,1).*NaN;
            end
        end
        
        function latexlabel_=latexlabel(A_)
            %check if folder basename_eval exists:
            [filepath,filename,ext]=fileparts(A_.filename);
            cd(fileparts(A_.filename))
            evalfolder=[filename,'_eval'];
            if exist(evalfolder,'dir')==7
                %waitfor(msgbox(['The folder ',evalfolder,' exists.']))
                cd(evalfolder)
            else
                %waitfor(msgbox(['The folder ',evalfolder,' does not exist.']))
            end
            if exist('latexlabel.txt','file')==2
                data=importdata('latexlabel.txt');
                latexlabel_=data{1};
            else
                latexlabel_=A_.label;
            end
        end
        
        
        
        function [ p ] = polyfitw(x,y,w,n)
            % This function is based on matlab's polyfit function:
            
            %POLYFIT Fit polynomial to data.
            %   P = POLYFIT(X,Y,N) finds the coefficients of a polynomial P(X) of
            %   degree N that fits the data Y best in a least-squares sense. P is a
            %   row vector of length N+1 containing the polynomial coefficients in
            %   descending powers, P(1)*X^N + P(2)*X^(N-1) +...+ P(N)*X + P(N+1).
            
            % It was modified such that it has an aditional input w, which assignes a
            % different weight to every data point. w needs to be of the same size as x
            % and y. The fitting is now done giving each point the corresponding
            % weight.
            
            % make sure that x,y and w are of the same size
            if ~isequal(size(x),size(y))
                error('MATLAB:polyfit:XYSizeMismatch',...
                    'X and Y vectors must be the same size.')
            end
            
            if ~isequal(size(x),size(w))
                error('MATLAB:polyfit:XYSizeMismatch',...
                    'X and W vectors must be the same size.')
            end
            
            % make sure that x,y and w are all column vectors.
            x = x(:);
            y = y(:);
            w = w(:);
            
            %constract w as a matrix
            W=zeros(length(x),length(x));
            for i=1:length(x)
                W(i,i)=w(i);
            end
            %W=W;
            
            % Construct Vandermonde matrix
            V(:,n+1) = ones(length(x),1,class(x));
            for j = n:-1:1
                V(:,j) = x.*V(:,j+1);
            end
            
            %V=V;
            coorm=(V'*W)*(V);
            %croom=(w*V')*(V*w')
            b=(V'*W) * (y);
            coorm^-1;
            % Solve least squares problem
            %p = ((V'*W)*(W*V))^-1 * (V'*W) * (W*y);
            p= coorm^-1 * b;
            
            p = p.';          % Polynomial coefficients are row vectors by convention.
            
        end
        
        
    end
    
end
