subplot(2,2,2)
%%
set(gca,'LineWidth',1);
set(gca,'FontSize',14);
xlabel(xlabel_,'Fontsize',24,'Interpreter','latex');
ylabel(ylabel_,'Fontsize',24,'Interpreter','latex');
title(graph_title_,'Interpreter','latex');
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'Units', 'centimeters');
set(gcf, 'Position', [2 2 27 17]);
set(gca, 'Units', 'centimeters');
%legend('Location','Best');
%set(gca, 'Position', [3 3 12 12]);
legend(gca,'show');
legend(gca,'boxoff');
LEGH = legend;
set(LEGH,'FontSize',10);
set(LEGH,'Interpreter','latex');
set(get(gca,'Children'),'linewidth',1); % This sets the linewidth for all plots. (plots are 'Children' of the current axis.
set(gcf,'PaperOrientation','landscape');
set(gcf,'PaperPositionMode','auto');


%%
            
            
% close all 
% % produce a few figures (3)
% figure(1)
% subplot(3,1,1)
% for k=1:3
%     subplot(3,1,k)
%     plot(rand(10,1))
% end
% figure(2)
% subplot(3,1,1)
% for k=1:3
%     subplot(3,1,k)
%     plot(rand(3,1), '-o')
% end
% figure(3)
% plot(rand(20,1))

% Collect all subplots from all figures 


%%

h1 = openfig('g1_vs_t_and_num_fit_100deg.fig','reuse'); % open figure
ax1 = gca; % get handle to axes of figure
h2 = openfig('g1_vs_t_and_num_fit_120deg.fig','reuse');
ax2 = gca;


hFigure = figure;                              %# Create a new figure
hTemp = subplot(2,1,1,'Parent',hFigure);         %# Create a temporary subplot
newPos = get(hTemp,'Position');                  %# Get its position
delete(hTemp);                                   %# Delete the subplot
set(h1,'Parent',hFigure,'Position',newPos);  %# Move axes to the new figure
                                                 %#   and modify its position
hTemp = subplot(2,1,2,'Parent',hFigure);         %# Make a new temporary subplot
%# ...repeat the above for fig(2)

%%

AllAxH=setdiff(findobj( '-property', 'children', '-depth' ,2),...
    findobj( '-property', 'children', '-depth' ,1));
% put all those subplots in a single new figure 
figure
for k=1:4
hsp=subplot(2,2,k);
copyobj(get(AllAxH(k), 'children'),hsp)
end