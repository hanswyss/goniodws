function f=f_to_minimize(x)

global t_exp;
global g1_exp;
global R;
global angle;
global lambda;

[s,Ps] = P_s_from_master_curve(R, x, angle);
N=length(t_exp);
f=0;
for ii=1:N
%    if not(isnan(g1_exp(ii)))
        if (g1_exp(ii)>0.1 && g1_exp(ii)<0.9 )
            g1_sim(ii)=g1_from_Ps(lambda,s,Ps,x,t_exp(ii));
            f=f+(log(g1_sim(ii))-log(g1_exp(ii)))^2;
        end
%    end
end

end
