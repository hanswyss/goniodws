function ah = nice_subplot(htarget,m,n,i,x,y,format,xlabel_,ylabel_)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
figure(htarget)
ah=subplot(m,n,i);
plot(x,y,format);
hold on

set(ah,'LineWidth',1);
set(ah,'FontSize',14);
xlabel(xlabel_,'Fontsize',24,'Interpreter','latex');
ylabel(ylabel_,'Fontsize',24,'Interpreter','latex');
%title(graph_title_,'Interpreter','latex');
% set(gcf, 'PaperUnits', 'centimeters');
% set(gcf, 'Units', 'centimeters');
% set(gcf, 'Position', [2 2 27 17]);
% set(ah, 'Units', 'centimeters');
% set(ah, 'Position', [3 3 12 12]);

% legend('Location','Best');
% legend(ah,'show');
% legend(ah,'boxoff');
% LEGH = legend;
% set(LEGH,'FontSize',10);
% set(LEGH,'Interpreter','latex');
set(get(ah,'Children'),'linewidth',1); % This sets the linewidth for all plots. (plots are 'Children' of the current axis.
% set(gcf,'PaperOrientation','landscape');
% set(gcf,'PaperPositionMode','auto');



end

