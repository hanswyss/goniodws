function f=f_to_minimize_single_point(x)

global t_pt;
global g1_pt;
global R;
global angle;
global lambda;

[s,Ps] = P_s_from_master_curve(R, x, angle);


g1_sim_=g1_from_Ps(lambda,s,Ps,x,t_pt);
f=(g1_sim_-g1_pt)^2;

end

