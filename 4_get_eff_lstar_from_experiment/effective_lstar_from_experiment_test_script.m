filenr=0;
pathname='./';
filename=['dilu2_00',num2str(filenr+10),'_0001.ASC'];
%filename='ALV_DWS_dil2_0008.ASC';

fullname=[pathname,filename];
A=LSdata.ImportFromFile(fullname);



%%

g1_=A.g1_scaled;
t_=A.t_s;

%%
figure(1);
semilogx(A.t_s,A.g1_scaled);
hold all
graph_title_='Field correlation function (scaled).';
xlabel_='$t$ [s]';
ylabel_='$g_1(t)$';
legend_=['$\theta=',num2str(A.angle_deg),'^{\circ}$'];
legend(legend_)
LSdata.nicify_plot(gcf,graph_title_,xlabel_,ylabel_)
axis([1e-8 1e-2 0 1.1])
saveas(gcf,'g1_vs_t','pdf');
saveas(gcf,'g1_vs_t','fig');

%%
figure(2);clf;figure(2);
x=A.t_s;
y=log(A.g1_scaled);
range=(A.g1_scaled>0);x=x(range);y=y(range);
range=(x<2e-4);x=x(range);y=y(range);
plot(x,y,'ko');
graph_title_='Field correlation function (scaled).';
xlabel_='$t$ [s]';
ylabel_='$\log\left(g_1(t)\right)$';
fit1=polyfit(x(x<1e-5),y(x<1e-5),1)
hold on
one_=ones(size(x));
plot(x,fit1(1).*x + fit1(2)*one_ )
legend_={['$\theta=',num2str(A.angle_deg),'^{\circ}$'],['linear fit, slope: $\Gamma=$',num2str(fit1(1),'%e')]};
legend(legend_)
LSdata.nicify_plot(gcf,graph_title_,xlabel_,ylabel_)
axis([0 1e-4 -1 0.05])
saveas(gcf,'g1_vs_t','pdf');
saveas(gcf,'g1_vs_t','fig');

%%


global t_exp;
global g1_exp;
global R;
global angle;
global lambda;

t_exp=A.t_s;
g1_exp=A.g1_scaled;
R=0.005;
angle=A.angle_deg;
lambda=A.lambda_m;

range=(g1_exp>0.1 & g1_exp<0.95);
t_exp=A.t_s(range);
g1_exp=A.g1_scaled(range);

lstar=500e-6;
[s,Ps] = P_s_from_master_curve(R, lstar, angle);

%% Find the optimal lstar value via a least squares fit:
x0=1200e-6;
x = fminsearch(@f_to_minimize, x0);

lstar_opt=x;
%lstar_opt=2000e-6

%%
%fit decay rates:
% range=(g1_exp>0.2 & g1_exp<0.99);
% figure(21)
% clf
% figure(21)
% semilogx(t_exp(range),g1_exp(range),'o')
% hold on
% x=t_exp
% semilogx(x,myFit.a*exp(myFit.b*x),'-')
% %%
% fitType = 'exp1';
% myFit = fit(t_exp(range),g1_exp(range),fitType);
% Gamma=-myFit.b
% 
% %p=polyfit(t_exp(range),-log(g1_exp(range)),1);
% %Gamma=p(1);
% tau_exp=1/Gamma;
% %p=polyfit(t_exp(range),-log(g1_exp(range)),1);
% %Gamma=p(1);
% tau_sim=1/Gamma;
% lstar_opt=tau_exp/tau_sim*lstar_opt
%%

figure(7)
clf
figure(7)

t_exp=A.t_s;
g1_exp=A.g1_scaled;

N=length(g1_exp);
g1_sim=zeros(size(g1_exp));
for ii=1:N
    g1_sim(ii)=g1_from_Ps(lambda,s,Ps,lstar_opt,t_exp(ii));
end



semilogx(t_exp,g1_exp,'ok')
hold on
semilogx(t_exp,g1_sim,'-r');
graph_title_='Calculated and experimental correlation function';
xlabel_='$t$';
ylabel_='$g_1(t)$';

legend_={['experiment, $\theta=$',num2str(angle),'$^\circ$'],['calculation with $l^\star=$',num2str(lstar_opt*1e6),'$\mu$m']};
legend(legend_)
LSdata.nicify_plot(gcf,graph_title_,xlabel_,ylabel_)
axis([1e-6 1e-2 0 1.05])
outfile=['g1_vs_t_and_num_fit_',num2str(angle),'deg'];
saveas(gcf,outfile,'fig');
saveas(gcf,outfile,'pdf');
export_fig g1_vs_t_and_num_fit.pdf -transparent


%%
figure(8)
clf
figure(8)
plot(t_exp,log(g1_exp),'o')
hold on
plot(t_exp,log(g1_sim),'-')
axis([0 4e-4 -2.5 0])
graph_title_='Calculated and experimental correlation function';
xlabel_='$t$'
ylabel_='$\log(g_1(t))$'

legend_={['experiment, $\theta=$',num2str(angle),'$^\circ$'],['calculation with $l^\star=$',num2str(lstar_opt*1e6),'$\mu$m']};
legend(legend_)
LSdata.nicify_plot(gcf,graph_title_,xlabel_,ylabel_)
export_fig 'log_g1_vs_t_and_num_fit.pdf' -transparent


%% optimal lstar vs. t


global t_pt;
global g1_pt;


t_exp=A.t_s;
g1_exp=A.g1_scaled;
R=0.005;
angle=A.angle_deg;
lambda=A.lambda_m;

%% Find the optimal lstar value via a least squares fit:
for ii=1:N
    g1_pt=g1_exp(ii);
    t_pt=t_exp(ii);
    if not(isnan(g1_pt))
        x0=500e-6;
        lstar_(ii) = fminsearch(@f_to_minimize_single_point, x0);
    else
        lstar_(ii)=NaN;
    end
end


%%
figure(9)
loglog(t_exp,lstar_)

%% record values of lstar vs. angle
angle_filenr(filenr+1)=A.angle_deg;
lstar_filenr(filenr+1)=lstar_opt;
figure(11)
clf
figure(11)
semilogy(angle_filenr,lstar_filenr*1e6,'o')
hold on
%axis([40 160 100 1000])
graph_title_='Fitted $l^\star$ as a function of detection angle';
xlabel_='Detection angle $\theta$ [$^\circ$]'
ylabel_='$l^\star$ [$\mu$m]'

legend_={'dilution 2; $\phi=0.625\%$'};
legend(legend_)
LSdata.nicify_plot(gcf,graph_title_,xlabel_,ylabel_)
outname='Fitted_lstar_vs_theta';
saveas(gcf,outname,'fig')
saveas(gcf,outname,'pdf')

p=polyfit(angle_filenr,lstar_filenr*1e6,1);
x=angle_filenr;
semilogy(x,p(1)*x+p(2),'r-')

export_fig Fitted_lstar_vs_detection_angle.pdf -transparent


