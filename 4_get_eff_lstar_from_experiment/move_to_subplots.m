function []= move_to_subplots(ax,a,b)
%     %
% Inputs:
%       inputname: 
% Outputs:
%       name:  description type units
%       saved data: (does this overwrite a statically named file?)
%       plots:
%
% Standard call:
%
%
% Written by C. Hogg Date 2012_06_01
%
% 
debugmode=0;

hFigure=figure();

if ~exist('a')
        a=ceil(sqrt(length(ax)));
end

if ~exist('b')
        b=1;
    end    

if a*b<length(ax)|~exist('a')|~exist('b')
    disp('Auto subplot sizing')

    b=ceil(length(ax)/a);
end

for i=1:length(ax)

hTemp = subplot(a,b,i,'Parent',hFigure);         %# Make a new temporary subplot
newPos = get(hTemp,'Position');                  %# Get its position
delete(hTemp);

hNew = copyobj(ax(i),hFigure);
set(hNew,'Position',newPos)
end

%% Debug. Break point here.
if debugmode==1; dbstop tic; tic; dbclear all;end

end

