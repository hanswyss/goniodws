function ah = nicify_subplot(m,n,i,xlabel_,ylabel_)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
figure(gcf)
ah=subplot(m,n,i);



set(ah,'LineWidth',1);
set(ah,'FontSize',14);
xlabel(xlabel_,'Fontsize',24,'Interpreter','latex');
ylabel(ylabel_,'Fontsize',24,'Interpreter','latex');
%title(graph_title_,'Interpreter','latex');
% set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'Units', 'centimeters');
pos=get(gcf, 'Position');
%newsize=(pos(3)+pos(4))/2
newsize=25;
pos(3)=newsize;pos(4)=newsize;
set(gcf, 'Position', pos);

pos=get(ah, 'Position');
newsize=(pos(3)+pos(4))/2;
pos(3)=newsize;pos(4)=newsize;
set(ah, 'Position', pos);

% set(ah, 'Units', 'centimeters');
% set(ah, 'Position', [3 3 12 12]);

% legend('Location','Best');
% legend(ah,'show');
% legend(ah,'boxoff');
% LEGH = legend;
% set(LEGH,'FontSize',10);
% set(LEGH,'Interpreter','latex');
set(get(ah,'Children'),'linewidth',1); % This sets the linewidth for all plots. (plots are 'Children' of the current axis.
% set(gcf,'PaperOrientation','landscape');
% set(gcf,'PaperPositionMode','auto');

legend(ah,'show');
%legend(ah,'boxoff');
LEGH = legend;
set(LEGH, 'XColor', 'w', 'YColor', 'w', 'Color', 'none');
set(LEGH,'FontSize',10);
set(LEGH,'Interpreter','latex');
set(LEGH,'Location','Best');


switch i
    
    case 1
        text(-0.1,1,'A','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 2
        text(-0.1,1,'B','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 3
        text(-0.1,1,'C','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
    case 4
        text(-0.1,1,'D','FontSize',30,'Units', 'Normalized', 'VerticalAlignment', 'Bottom', 'HorizontalAlignment', 'Left')
end

