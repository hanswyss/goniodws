function figure_to_file( fh,filename )
%fh: figure handle
%filename: filename, including extension

ah=get(fh,'CurrentAxes')
set(ah,'units','centimeters')
pos = get(ah,'Position');
ti = get(ah,'TightInset');

set(fh, 'PaperUnits','centimeters');
set(fh, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
set(fh, 'PaperPositionMode', 'manual');
set(fh, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);

saveas(gcf,filename)

end

