function g1_t=g1_from_Ps(lambda,s,Ps,lstar,t)
    N=length(s);
    % Integrate over path length distribution
    g1_t=0;
    k0=2*pi/lambda;
    kT=4e-21;
    r=0.5e-6;
    eta=1e-3;
    MSD_t=2*kT/6/pi/eta/r.*t;
    for ii=2:N
        g1_t=g1_t+(s(ii)-s(ii-1))*(Ps(ii)+Ps(ii-1))/2.*exp(-k0^2*MSD_t*s(ii)/lstar);
    end
end